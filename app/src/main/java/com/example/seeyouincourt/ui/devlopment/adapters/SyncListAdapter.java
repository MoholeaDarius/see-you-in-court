package com.example.seeyouincourt.ui.devlopment.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.model.Sync;
import com.example.seeyouincourt.model.constants.Status;
import com.example.seeyouincourt.util.DateUtil;

import java.util.ArrayList;

public class SyncListAdapter extends ArrayAdapter {

    private final Context mContext;
    private final int mResource;

    public SyncListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Sync> syncsList) {
        super(context, resource, syncsList);
        mContext = context;
        mResource = resource;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint({"ViewHolder", "SetTextI18n"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(mResource, parent, false);
        Sync sync = (Sync) getItem(position);

        TextView syncIdTxt = convertView.findViewById(R.id.sync_id_value_txt);
        syncIdTxt.setText(sync.getId() + "");

        TextView syncStartDate = convertView.findViewById(R.id.sync_startDate_value_txt);
        syncStartDate.setText(sync.getStartDate().format(DateUtil.dateTimeFormatter));

        TextView syncEndDate = convertView.findViewById(R.id.sync_endDate_value_txt);
        if (sync.getEndDate() != null) {
            syncEndDate.setText(sync.getEndDate().format(DateUtil.dateTimeFormatter));
        }
        
        TextView syncStatus = convertView.findViewById(R.id.sync_status_value_txt);
        syncStatus.setText(sync.getStatus().getValue());
        syncStatus.setTextColor(sync.getStatus().equals(Status.FAIL) ? Color.RED : Color.GREEN);

        return convertView;
    }
}
