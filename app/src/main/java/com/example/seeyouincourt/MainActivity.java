package com.example.seeyouincourt;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.res.Configuration;

import android.os.Build;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.seeyouincourt.model.constants.Status;
import com.example.seeyouincourt.service.SyncService;
import com.example.seeyouincourt.ui.Sync.SyncFragment;
import com.example.seeyouincourt.ui.about_us.AboutUsFragment;
import com.example.seeyouincourt.ui.devlopment.DevFragment;
import com.example.seeyouincourt.ui.devlopment.security.DevSecurityFragment;
import com.example.seeyouincourt.ui.dossiers.DossierFragmentLandscapeContainer;
import com.example.seeyouincourt.ui.dossiers.DossierFragment;
import com.example.seeyouincourt.ui.dossiers.tools.DeviceConfig;
import com.example.seeyouincourt.ui.dossiers.tools.SingletonDataContainer;
import com.example.seeyouincourt.ui.meetings.MeetingsFragment;

import com.google.android.material.navigation.NavigationView;

import java.util.Objects;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mainLayout;
    private Toolbar toolbar;
    private NavigationView navigationView;

    private double screenDiagonalInches;
    private int screenOrientation;

    private MeetingsFragment meetingsFragment;
    private DevSecurityFragment devSecurityFragment;
    private DossierFragment dossierFragment;
    private DossierFragmentLandscapeContainer dossierFragmentLandscapeContainer;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        screenDiagonalInches = DeviceConfig.getScreenDiagonal(getWindowManager());
        screenOrientation = getResources().getConfiguration().orientation;
        initViews();
        setDossiersFragmentAsDefaultFragment();

        setSupportActionBar(toolbar);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mainLayout, toolbar, R.string.nav_draw_open, R.string.nav_draw_close);
        mainLayout.addDrawerListener(toggle);

        SyncService syncService = SyncService.getInstance(MainActivity.this);
        if (syncService.getLastSync().getStatus() == null || syncService.getLastSync().getStatus().equals(Status.SUCCESS)) {
            findViewById(R.id.sync_failed_main).setVisibility(View.GONE);
        }
        toggle.syncState();
    }

    private void initViews() {
        toolbar = findViewById(R.id.main_toolbar);
        mainLayout = findViewById(R.id.main_layout);
        navigationView = findViewById(R.id.nav_view);
        dossierFragmentLandscapeContainer = new DossierFragmentLandscapeContainer();
        dossierFragment = new DossierFragment();
        meetingsFragment = new MeetingsFragment();
        devSecurityFragment = new DevSecurityFragment();
    }

    private void setDossiersFragmentAsDefaultFragment() {
        if (screenOrientation == Configuration.ORIENTATION_LANDSCAPE
                && screenDiagonalInches > DeviceConfig.AVERAGE_TABLET_DIAGONAL) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_fragment_container, dossierFragmentLandscapeContainer)
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_fragment_container, dossierFragment)
                    .commit();
        }
        navigationView.setCheckedItem(R.id.dossier_nav_item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.dossier_nav_item:
                if (screenOrientation == Configuration.ORIENTATION_LANDSCAPE
                        && screenDiagonalInches > DeviceConfig.AVERAGE_TABLET_DIAGONAL) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.main_fragment_container, dossierFragmentLandscapeContainer)
                            .commit();
                } else {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.main_fragment_container, dossierFragment)
                            .commit();
                }
                break;

            case R.id.meetings_nav_item:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_fragment_container, meetingsFragment)
                        .commit();
                break;

            case R.id.about_us_nav_item:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_fragment_container, new AboutUsFragment())
                        .commit();
                break;

            case R.id.sync_nav_item:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_fragment_container, new SyncFragment(MainActivity.this))
                        .commit();
                break;

            case R.id.dev_nav_item:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_fragment_container, devSecurityFragment, "Security fragment!")
                        .commit();
                break;
        }
        mainLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        switch (Objects.requireNonNull(navigationView.getCheckedItem()).getItemId()) {
            case R.id.dossier_nav_item:
                if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE
                        && screenDiagonalInches > DeviceConfig.AVERAGE_TABLET_DIAGONAL) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.main_fragment_container, dossierFragmentLandscapeContainer)
                            .commit();
                } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT && screenDiagonalInches > 6.5
                        && SingletonDataContainer.getInstance().getDossierDetailsIntent() != null) {
                    SingletonDataContainer.getInstance().setGoInDossiersList(true);
                    startActivity(SingletonDataContainer.getInstance().getDossierDetailsIntent());
                } else {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.main_fragment_container, dossierFragment)
                            .commit();
                }
                break;

            case R.id.meetings_nav_item:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_fragment_container, meetingsFragment)
                        .commit();
                break;

            case R.id.about_us_nav_item:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_fragment_container, new AboutUsFragment())
                        .commit();
                break;

            case R.id.sync_nav_item:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_fragment_container, new SyncFragment(MainActivity.this))
                        .commit();
                break;

            case R.id.dev_nav_item:
                DevSecurityFragment devSecurityFragment = (DevSecurityFragment) getSupportFragmentManager().findFragmentByTag("Security fragment!");
                if (devSecurityFragment != null && devSecurityFragment.isVisible()) {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.main_fragment_container, devSecurityFragment)
                            .commit();
                } else {
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.main_fragment_container, new DevFragment())
                            .commit();
                }
                break;
        }
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        if (screenDiagonalInches > DeviceConfig.AVERAGE_TABLET_DIAGONAL && SingletonDataContainer.getInstance().isGoInDossiersListActive()) {
            setContentView(R.layout.activity_main);
            screenDiagonalInches = DeviceConfig.getScreenDiagonal(getWindowManager());
            screenOrientation = getResources().getConfiguration().orientation;
            initViews();

            setDossiersFragmentAsDefaultFragment();

            setSupportActionBar(toolbar);
            navigationView.setNavigationItemSelectedListener(this);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mainLayout, toolbar, R.string.nav_draw_open, R.string.nav_draw_close);
            mainLayout.addDrawerListener(toggle);
            toggle.syncState();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBackPressed() {
        if (mainLayout.isDrawerOpen(GravityCompat.START)) {
            mainLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_cab_menu, menu);
        MenuItem actionSearchItem = menu.findItem(R.id.search_cab_action);
        SearchView searchView = (SearchView) actionSearchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                dossierFragment.getAdapter().getFilter().filter(newText);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

}