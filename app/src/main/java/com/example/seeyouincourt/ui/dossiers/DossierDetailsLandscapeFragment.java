package com.example.seeyouincourt.ui.dossiers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.ui.dossiers.adapters.DossierDetailsFragmentAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

/**
 * this is fragment with dossier details used by tablets in landscape mode
 * Contains a view pager
 */
public class DossierDetailsLandscapeFragment extends Fragment {

    private TabLayout tabLayout;

    private ViewPager2 viewPager;

    private List<Dossier> dossiers;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dossier_details_landscape_fragment, container, false);

        initViews(view);
        viewPager.setAdapter(getFragmentAdapter(dossiers));
        addTabsToTabLayout(dossiers);
        selectedTabListener();

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });
        return view;
    }

    private void initViews(View view) {
        tabLayout = view.findViewById(R.id.dossier_details_fragment_tab_layout);
        viewPager = view.findViewById(R.id.dossier_details_fragment_view_pager);
    }

    private DossierDetailsFragmentAdapter getFragmentAdapter(List<Dossier> dossiers) {
        return new DossierDetailsFragmentAdapter(getChildFragmentManager(), getLifecycle(), dossiers);
    }

    private void addTabsToTabLayout(List<Dossier> dossiers) {
        for (Dossier dossier : dossiers) {
            tabLayout.addTab(tabLayout.newTab().setText(dossier.getCourt().getValue()));
        }
    }

    private void selectedTabListener() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void setDossiers(List<Dossier> dossiers) {
        this.dossiers = dossiers;
    }
}
