package com.example.seeyouincourt.dao.constants;

import com.example.seeyouincourt.dao.DossierDaoImpl;

/**
 * Local enum for {@link DossierDaoImpl}.
 * Contains column names and a quarry (create table)
 */

public enum DossierProperties {

    TABLE_NAME("dossiers"),
    ID("id"),
    NAME("name"),
    NUMBER("number"),
    OLD_NUMBER("oldNumber"),
    DATE("date"),
    COURT("court"),
    DEPARTMENT("department"),
    CASE_CATEGORY("caseCategory"),
    PROCESS_STAGE("processStage"),
    OBJECT("object"),
    MODIFICATION_DATE("modificationDate"),
    CASE_CATEGORY_NAME("caseCategoryName"),
    PROCESS_STAGE_NAME("processStageName"),
    CURRENT_NAME("current"),

    CREATE_TABLE_SQL("CREATE TABLE " + TABLE_NAME.getValue() + " ("
            + ID.getValue() + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + NAME.getValue() + " TEXT,"
            + NUMBER.getValue() + " TEXT,"
            + OLD_NUMBER.getValue() + " TEXT,"
            + DATE.getValue() + " TEXT,"
            + COURT.getValue() + " TEXT,"
            + DEPARTMENT.getValue() + " TEXT,"
            + CASE_CATEGORY.getValue() + " TEXT,"
            + PROCESS_STAGE.getValue() + " TEXT,"
            + OBJECT.getValue() + " TEXT,"
            + MODIFICATION_DATE.getValue() + " TEXT,"
            + CASE_CATEGORY_NAME.getValue() + " TEXT,"
            + PROCESS_STAGE_NAME.getValue() + " TEXT,"
            + CURRENT_NAME.getValue() + " INTEGER)");

    private final String value;

    DossierProperties(String v) {
        value = v;
    }

    public String getValue() {
        return value;
    }

    public static DossierProperties fromValue(String v) {
        for (DossierProperties dP : DossierProperties.values()) {
            if (dP.getValue().equalsIgnoreCase(v)) {
                return dP;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
