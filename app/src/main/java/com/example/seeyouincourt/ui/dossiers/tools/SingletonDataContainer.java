package com.example.seeyouincourt.ui.dossiers.tools;

import android.content.Intent;
import android.view.ActionMode;

import com.example.seeyouincourt.model.Dossier;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton design pattern
 * "Global container" - Used to store useful data for a short period of time
 */
public final class SingletonDataContainer {

    private static SingletonDataContainer instance;

    private int currentSelectedDossierPos;

    private Intent dossierDetailsIntent;

    private boolean goInDossiersList;

    private SingletonDataContainer() {
    }

    public static SingletonDataContainer getInstance() {
        if (instance == null) {
            instance = new SingletonDataContainer();
        }

        return instance;
    }

    public int getCurrentSelectedDossierPos() {
        return currentSelectedDossierPos;
    }

    public void setCurrentSelectedDossierPos(int currentSelectedDossierPos) {
        this.currentSelectedDossierPos = currentSelectedDossierPos;
    }

    public Intent getDossierDetailsIntent() {
        return dossierDetailsIntent;
    }

    public void setDossierDetailsIntent(Intent dossierDetailsIntent) {
        this.dossierDetailsIntent = dossierDetailsIntent;
    }

    public void setGoInDossiersList(boolean goInDossiersList) {
        this.goInDossiersList = goInDossiersList;
    }

    public boolean isGoInDossiersListActive() {
        return goInDossiersList;
    }
}
