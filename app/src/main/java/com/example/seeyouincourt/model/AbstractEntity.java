package com.example.seeyouincourt.model;

import java.io.Serializable;
import java.util.Objects;

/**
 * Class AbstractEntity is the root for all persisted entities.
 */
public abstract class AbstractEntity implements Serializable {

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isPersisted() {
        return id != 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractEntity that = (AbstractEntity) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
