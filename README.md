## What is this repository for? ##

Specialized application for notifying lawyers about court hearings.

## How do I get set up? ##

#### JDK version: ####
<code> You need to have installed JDK 1.8</code> 
#### Build tool: ####
<code> Gradle 7.0.2</code> 
#### Dependencies: ####
<code> 
     androidx.appcompat:appcompat:1.3.1
<br>     com.google.android.material:material:1.4.0
<br>     androidx.constraintlayout:constraintlayout:2.1.1
<br>     lib\\ksoap2-android-assembly-3.6.4-jar-with-dependencies.jar
<br>    androidx.legacy:legacy-support-v4:1.0.0
<br>     androidx.test.ext:junit:1.1.3
<br>     androidx.test.espresso:espresso-core:3.4.0
<br>     com.github.Spikeysanju:MotionToast:1.4
<br>     com.github.dierivera:Collapsible-Calendar-View-Android:v1.0.2
</code>

## Who do I talk to? ##

If you have any questions or trouble running the application, contact me @ <code> darius.moholea.dev@gmail.com</code> 