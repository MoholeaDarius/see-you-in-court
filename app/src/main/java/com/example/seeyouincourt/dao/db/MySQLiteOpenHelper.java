package com.example.seeyouincourt.dao.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.seeyouincourt.dao.constants.DossierProperties;
import com.example.seeyouincourt.dao.constants.MeetingProprieties;
import com.example.seeyouincourt.dao.constants.SyncProperties;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "SeeYouInCourtDB";
    private static final int DB_VERSION = 1;

    public MySQLiteOpenHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DossierProperties.CREATE_TABLE_SQL.getValue());
        db.execSQL(MeetingProprieties.CREATE_TABLE_SQL.getValue());
        db.execSQL(SyncProperties.CREATE_TABLE_SQL.getValue());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

}
