package com.example.seeyouincourt.service;

import static com.example.seeyouincourt.service.util.DossierUtil.getLastModificationDateDossier;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.seeyouincourt.dao.DossierDao;
import com.example.seeyouincourt.dao.DossierDaoImpl;
import com.example.seeyouincourt.dao.MeetingDao;
import com.example.seeyouincourt.dao.MeetingDaoImpl;
import com.example.seeyouincourt.dao.SyncDao;
import com.example.seeyouincourt.dao.SyncDaoImpl;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.model.Sync;
import com.example.seeyouincourt.model.constants.Status;
import com.example.seeyouincourt.service.portal.DossierWebClientImpl;
import com.example.seeyouincourt.exception.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

public class SyncService {

    private static SyncService instance;

    private static final String DOSSIER_NUMBER_FOR_PORTAL_TESTING = "6512/112/2012";

    private final DossierDao dossierDao;
    private final MeetingDao meetingDao;
    private final SyncDao syncDao;

    private final DossierWebClientImpl dossierWebClient;

    private SyncService(Context context) {
        this.dossierDao = DossierDaoImpl.getInstance(context);
        this.meetingDao = MeetingDaoImpl.getInstance(context);
        this.syncDao = SyncDaoImpl.getInstance(context);
        this.dossierWebClient = new DossierWebClientImpl();
    }

    public static SyncService getInstance(Context context) {
        if (instance == null) {
            instance = new SyncService(context);
        }
        return instance;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void syncDossiers() {
        Sync sync = new Sync();
        sync.setStartDate(LocalDateTime.now());
        sync.setStatus(Status.FAIL);
        sync.setId(syncDao.add(sync));

        if (isPortalAvailable()) {
            meetingDao.deleteAll();
            updateDossiers(dossierDao.get().orElse(new ArrayList<>()));

            sync.setEndDate(LocalDateTime.now());
            sync.setStatus(Status.SUCCESS);
            syncDao.update(sync);
        }
    }

    private boolean isPortalAvailable() {
        return !getDossiersByDossierNoFromPortal(DOSSIER_NUMBER_FOR_PORTAL_TESTING).isEmpty();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void updateDossiers(List<Dossier> oldDossiers) {
        if (oldDossiers == null || oldDossiers.isEmpty()) {
            return;
        }
        Map<String, List<Dossier>> updatedDossiersMap = new HashMap<>();
        for (Dossier oldDossier : oldDossiers) {
            // populate map with dossiers from portal
            if (updatedDossiersMap.get(oldDossier.getNumber()) == null) {
                List<Dossier> dossiersFromPortal = getDossiersByDossierNoFromPortal(oldDossier.getNumber());
                // set current
                setCurrent(dossiersFromPortal);
                updatedDossiersMap.put(oldDossier.getNumber(), dossiersFromPortal);
            }

            // update dossier and meetings in db
            List<Dossier> dossiersFromPortal = updatedDossiersMap.get(oldDossier.getNumber());
            dossiersFromPortal.forEach(updatedDossierFromMap -> {
                if (oldDossier.getCourt().equals(updatedDossierFromMap.getCourt())) {
                    updatedDossierFromMap.setId(oldDossier.getId());
                    updatedDossierFromMap.setName(oldDossier.getName());
                    dossierDao.update(updatedDossierFromMap);
                    meetingDao.add(updatedDossierFromMap.getMeetings(), updatedDossierFromMap.getId());
                }
            });
        }
        //TODO: add new dossiers to db
        updatedDossiersMap.values().forEach(dossiers -> dossiers.stream()
                .filter(dossier -> !dossier.isPersisted())
                .forEach(dossierDao::add));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setCurrent(List<Dossier> dossiersFromPortal) {
        Dossier currentDossier = getLastModificationDateDossier(dossiersFromPortal);
        dossiersFromPortal.forEach(dossier -> dossier.setCurrent(dossier.getCourt().equals(currentDossier.getCourt())));
    }

    private List<Dossier> getDossiersByDossierNoFromPortal(String dossierNumber) {
        try {
            return new CallSearchDossier1(dossierNumber).execute().get();
        } catch (ExecutionException | InterruptedException e) {
            return new ArrayList<>();
        }
    }

    private class CallSearchDossier1 extends AsyncTask<Void, Void, List<Dossier>> {

        private String dossierNumber;

        public CallSearchDossier1(String dossierNumber) {
            this.dossierNumber = dossierNumber;
        }

        @Override
        protected List<Dossier> doInBackground(Void... voids) {
            try {
                return dossierWebClient.getDossiers(Objects.requireNonNull(dossierNumber));
            } catch (JustPortalException e) {
                return new ArrayList<>();
            }
        }

    }

    public Optional<List<Sync>> getSyncList() {
        return syncDao.get();
    }

    public Sync getLastSync() {
        return syncDao.getLast();
    }

    public void deleteAllSyncs() {
        syncDao.deleteAll();
    }
}
