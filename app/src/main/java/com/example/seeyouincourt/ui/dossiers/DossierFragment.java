package com.example.seeyouincourt.ui.dossiers;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.service.DossierService;
import com.example.seeyouincourt.ui.dossiers.adapters.DossierListAdapter;
import com.example.seeyouincourt.ui.dossiers.dialogs.AddDossierDialog;
import com.example.seeyouincourt.ui.dossiers.dialogs.CallBack;
import com.example.seeyouincourt.ui.dossiers.tools.DeviceConfig;
import com.example.seeyouincourt.ui.dossiers.tools.SingletonDataContainer;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Dossier fragment contains a list of dossiers.
 */
public class DossierFragment extends Fragment implements CallBack {

    private ListView customListView;
    private FloatingActionButton floatingAddButton;
    private List<Dossier> dossiers;
    private DossierListAdapter adapter;
    private DossierService service;
    private double screenDiagonalInches;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dossiers_fragment_portrait, container, false);
        initViews(view);

        service = DossierService.getInstance(requireContext());
        int currentSelectedDossierPos = SingletonDataContainer.getInstance().getCurrentSelectedDossierPos();
        screenDiagonalInches = DeviceConfig.getScreenDiagonal(requireActivity().getWindowManager());

        dossiers = service.getCurrentDossiers();

        Intent intent = new Intent(getContext(), DossierDetailsActivity.class);

        if (!dossiers.isEmpty() && DeviceConfig.isInLandscapeMode(getResources()) && screenDiagonalInches > DeviceConfig.AVERAGE_TABLET_DIAGONAL) {
            DossierDetailsLandscapeFragment dossierDetailsLandscapeFragment = new DossierDetailsLandscapeFragment();
            dossierDetailsLandscapeFragment.setDossiers(getDossiersByPosition(currentSelectedDossierPos));
            getParentFragmentManager()
                    .beginTransaction()
                    .replace(R.id.dossiers_details_container, dossierDetailsLandscapeFragment)
                    .commit();
            intent.putExtra("dossiers", (ArrayList<Dossier>) getDossiersByPosition(currentSelectedDossierPos));
            SingletonDataContainer.getInstance().setDossierDetailsIntent(intent);
            customListView.setItemChecked(currentSelectedDossierPos, true);
        }

        customListView.setOnItemClickListener((parent, view1, position, id) -> {
            intent.putExtra("dossiers", (ArrayList<Dossier>) getDossiersByPosition(position));
            if (screenDiagonalInches > DeviceConfig.AVERAGE_TABLET_DIAGONAL && DeviceConfig.isInLandscapeMode(getResources())) {
                DossierDetailsLandscapeFragment dossierDetailsLandscapeFragment = new DossierDetailsLandscapeFragment();
                dossierDetailsLandscapeFragment.setDossiers(getDossiersByPosition(position));
                getParentFragmentManager()
                        .beginTransaction()
                        .replace(R.id.dossiers_details_container, dossierDetailsLandscapeFragment)
                        .commit();
            } else {
                startActivity(intent);
            }
            SingletonDataContainer.getInstance().setCurrentSelectedDossierPos(position);
            SingletonDataContainer.getInstance().setDossierDetailsIntent(intent);
        });


        floatingAddButton.setOnClickListener(e -> {
            AddDossierDialog addDossierDialog = new AddDossierDialog();
            addDossierDialog.setTargetFragment(this, 1);
            addDossierDialog.show(getParentFragmentManager(), null);
        });

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private List<Dossier> getDossiersByPosition(int currentSelectedDossierPos) {
        return service.getDossiersByNo(dossiers.get(currentSelectedDossierPos).getNumber());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onStart() {
        super.onStart();
        dossiers = service.getCurrentDossiers();
        adapter = new DossierListAdapter(requireContext(), R.layout.dossier_custom_list_item, (ArrayList<Dossier>) dossiers);
        customListView.setAdapter(adapter);
    }

    private void initViews(View view) {
        floatingAddButton = view.findViewById(R.id.add_dossier_floating_btn);
        customListView = view.findViewById(R.id.custom_list_view);
        customListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        customListView.setMultiChoiceModeListener(modeListener);
    }

    private AbsListView.MultiChoiceModeListener modeListener = new AbsListView.MultiChoiceModeListener() {
        @Override
        public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {
            adapter.setLongPressedItemPos(i);
            adapter.setDossiersToDelete(new ArrayList<>());
        }

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.getMenuInflater().inflate(R.menu.contextual_action_bar, menu);
            adapter.setActionMode(true);
            adapter.setContextualActionMode(actionMode);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return true;
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            service.deleteDossiers(adapter.getDossiersToDelete());
            for (Dossier dossier : adapter.getDossiersToDelete()) {
                dossiers.remove(dossier);
                adapter.notifyDataSetChanged();
            }
            actionMode.finish();
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            adapter.setActionMode(false);
            adapter.setContextualActionMode(null);
            adapter.setDossiersToDelete(new ArrayList<>());
        }
    };

    /**
     * @param dossiers - this are dossiers that user want in his list
     *                 -we add them in database
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDossiersSent(List<Dossier> dossiers) {
        service.addNewDossiers(dossiers);

        this.dossiers.clear();
        this.dossiers.addAll(service.getCurrentDossiers());
        adapter.notifyDataSetChanged();
    }

    public ArrayAdapter getAdapter() {
        return adapter;
    }

}
