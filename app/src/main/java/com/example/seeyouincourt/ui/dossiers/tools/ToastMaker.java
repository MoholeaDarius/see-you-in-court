package com.example.seeyouincourt.ui.dossiers.tools;

import android.app.Activity;
import android.content.Context;

import androidx.core.content.res.ResourcesCompat;

import com.example.seeyouincourt.R;

import www.sanju.motiontoast.MotionToast;
import www.sanju.motiontoast.MotionToastStyle;

/**
 * This class provide custom toasts for various uses
 */
public class ToastMaker {

    public static void displayWarningToast(Activity requiredActivity, Context requiredContext, String toastMessage) {
        MotionToast.Companion.darkColorToast(requiredActivity,
                "",
                toastMessage,
                MotionToastStyle.WARNING,
                MotionToast.GRAVITY_BOTTOM,
                MotionToast.LONG_DURATION,
                ResourcesCompat.getFont(requiredContext, R.font.helvetica_regular));
    }

    public static void displayErrorToast(Activity requiredActivity, Context requiredContext, String toastMessage) {
        MotionToast.Companion.darkColorToast(requiredActivity,
                "",
                toastMessage,
                MotionToastStyle.ERROR,
                MotionToast.GRAVITY_BOTTOM,
                MotionToast.LONG_DURATION,
                ResourcesCompat.getFont(requiredContext, R.font.helvetica_regular));
    }


    public static void displaySuccessToast(Activity requiredActivity, Context requiredContext, String toastMessage) {
        MotionToast.Companion.darkColorToast(requiredActivity,
                "",
                toastMessage,
                MotionToastStyle.SUCCESS,
                MotionToast.GRAVITY_BOTTOM,
                MotionToast.LONG_DURATION,
                ResourcesCompat.getFont(requiredContext, R.font.helvetica_regular));
    }

}
