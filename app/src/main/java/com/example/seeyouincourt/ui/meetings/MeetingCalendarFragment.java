package com.example.seeyouincourt.ui.meetings;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.model.CalendarMeeting;
import com.example.seeyouincourt.service.DossierService;
import com.example.seeyouincourt.ui.meetings.adapter.MeetingAdapterForCalendar;

import com.example.seeyouincourt.util.DateUtil;
import com.shrikanthravi.collapsiblecalendarview.data.*;
import com.shrikanthravi.collapsiblecalendarview.widget.*;

import java.time.LocalDate;
import java.util.List;

public class MeetingCalendarFragment extends Fragment {

    private CollapsibleCalendar collapsibleCalendar;
    private RecyclerView recyclerView;
    private DossierService service;
    private List<CalendarMeeting> calendarMeetings;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.meeting_calendar_fragment, container, false);
        initView(view);
        this.service = DossierService.getInstance(requireContext());
        calendarMeetings = service.getCalendarMeetings();

        setupCalendar();

        return view;
    }

    private void initView(View view) {
        collapsibleCalendar = view.findViewById(R.id.calendar_id);
        recyclerView = view.findViewById(R.id.meetings_recycler_view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupCalendar() {
        addEventsForMonth(LocalDate.now().minusMonths(1).getMonthValue());
        addEventsForMonth(LocalDate.now().getMonthValue());
        addEventsForMonth(LocalDate.now().plusMonths(1).getMonthValue());

        collapsibleCalendar.setCalendarListener(new CollapsibleCalendar.CalendarListener() {

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDaySelect() {
                Day day = collapsibleCalendar.getSelectedDay();
                assert day != null;
                MeetingAdapterForCalendar meetingAdapter = new MeetingAdapterForCalendar(DateUtil.getCalendarMeetingsByDate(calendarMeetings, LocalDate.of(day.getYear(), day.getMonth() + 1, day.getDay())), requireContext());
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView.setAdapter(meetingAdapter);

            }

            @Override
            public void onItemClick(View v) {
            }

            @Override
            public void onDataUpdate() {

            }

            @Override
            public void onMonthChange() {
                addEventsForMonth(collapsibleCalendar.getMonth() + 1);
                addEventsForMonth(collapsibleCalendar.getMonth() + 2);
            }

            @Override
            public void onWeekChange(int i) {
                System.err.println("onWeekChange");
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void addEventsForMonth(int monthValue) {
        for (CalendarMeeting calendarMeeting : DateUtil.getCalendarMeetingsByMonth(calendarMeetings, monthValue)) {
            if (collapsibleCalendar.getYear() == calendarMeeting.getDate().getYear())
                collapsibleCalendar.addEventTag(calendarMeeting.getDate().getYear(),
                        calendarMeeting.getDate().getMonth().getValue() - 1,
                        calendarMeeting.getDate().getDayOfMonth());
        }
    }

}

