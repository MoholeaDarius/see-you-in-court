package com.example.seeyouincourt.ui.dossiers.tools;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.seeyouincourt.model.Dossier;

import java.util.ArrayList;
import java.util.List;

public class FilterTool {

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static List<Dossier> filter(List<Dossier> dossiers, String dossierData) {
        List<Dossier> filteredDossiersNumbers = new ArrayList<>();
        dossiers.forEach(dossier -> {
            if (dossier.getNumber().contains(dossierData)) {
                filteredDossiersNumbers.add(dossier);
            }

            if (dossier.getName() != null && dossier.getName().contains(dossierData)) {
                filteredDossiersNumbers.add(dossier);
            }

        });

        return filteredDossiersNumbers;
    }

}
