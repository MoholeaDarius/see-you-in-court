package com.example.seeyouincourt.dao.constants;

public enum SyncProperties {

    TABLE_NAME("SyncTable"),
    ID("id"),
    START_DATE("startDate"),
    END_DATE("endDate"),
    STATUS("status"),

    CREATE_TABLE_SQL("CREATE TABLE " + TABLE_NAME.getValue() + " ("
            + ID.getValue() + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + START_DATE.getValue() + " TEXT,"
            + END_DATE.getValue() + " TEXT,"
            + STATUS.getValue() + " TEXT)");

    private final String value;

    SyncProperties(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
