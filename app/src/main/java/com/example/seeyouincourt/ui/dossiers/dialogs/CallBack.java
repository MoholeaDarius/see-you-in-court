package com.example.seeyouincourt.ui.dossiers.dialogs;

import com.example.seeyouincourt.model.Dossier;

import java.util.List;

/**
 * Used to send data.
 */
public interface CallBack {

    void onDossiersSent(List<Dossier> dossiers);
}
