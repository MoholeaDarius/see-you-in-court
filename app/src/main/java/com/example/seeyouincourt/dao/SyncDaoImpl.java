package com.example.seeyouincourt.dao;

import static com.example.seeyouincourt.dao.constants.SyncProperties.*;
import static com.example.seeyouincourt.util.DateUtil.getDate;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.seeyouincourt.dao.db.MySQLiteOpenHelper;
import com.example.seeyouincourt.model.constants.Status;
import com.example.seeyouincourt.model.Sync;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SyncDaoImpl implements SyncDao {

    private static SyncDaoImpl instance;

    private final MySQLiteOpenHelper mySQLiteOpenHelper;

    private SyncDaoImpl(Context context) {
        this.mySQLiteOpenHelper = new MySQLiteOpenHelper(context);
    }

    public static SyncDaoImpl getInstance(Context context) {
        if (instance == null) {
            instance = new SyncDaoImpl(context);
        }
        return instance;
    }

    @SuppressLint("NewApi")
    public Optional<List<Sync>> get() {
        List<Sync> syncList = new ArrayList<>();
        SQLiteDatabase db = mySQLiteOpenHelper.getReadableDatabase();
        Cursor cursorCourses = db.rawQuery("SELECT * FROM " + TABLE_NAME.getValue(), null);
        if (cursorCourses.moveToFirst()) {
            do {
                syncList.add(buildSync(cursorCourses));
            } while (cursorCourses.moveToNext());
        }
        cursorCourses.close();
        return Optional.of(syncList);
    }


    @SuppressLint("NewApi")
    public Sync getLast() {
        SQLiteDatabase db = mySQLiteOpenHelper.getReadableDatabase();
        Cursor cursorCourses = db.rawQuery("SELECT * FROM " + TABLE_NAME.getValue() + " WHERE " +
                ID.getValue() + " =(SELECT max(" + ID.getValue() + ") FROM " + TABLE_NAME.getValue() + ");", null);
        if (cursorCourses.moveToFirst()) {
            return buildSync(cursorCourses);
        }
        cursorCourses.close();
        return new Sync();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public long add(Sync sync) {
        SQLiteDatabase db = mySQLiteOpenHelper.getWritableDatabase();
        long id = db.insert(TABLE_NAME.getValue(), null, getContentValues(sync));
        db.close();
        return id;
    }

    @Override
    public void update(Sync sync) {
        if (sync.getId() == 0) throw new IllegalStateException("Id is null!");
        SQLiteDatabase db = mySQLiteOpenHelper.getWritableDatabase();
        db.update(TABLE_NAME.getValue(), getContentValues(sync), String.format("%s = ?", "id"), new String[]{sync.getId() + ""});
        db.close();
    }

    public void deleteAll() {
        SQLiteDatabase db = mySQLiteOpenHelper.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME.getValue());
        db.close();
    }

    private ContentValues getContentValues(Sync sync) {
        ContentValues values = new ContentValues();
        if (sync.getStartDate() != null) {
            values.put(START_DATE.getValue(), sync.getStartDate().toString());
        }
        if (sync.getEndDate() != null) {
            values.put(END_DATE.getValue(), sync.getEndDate().toString());
        }
        values.put(STATUS.getValue(), sync.getStatus().toString());
        return values;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @NonNull
    private Sync buildSync(Cursor cursorCourses) {
        Sync sync = new Sync();
        sync.setId(cursorCourses.getInt(0));
        if (cursorCourses.getString(1) != null)
            sync.setStartDate(getDate(cursorCourses.getString(1)));
        if (cursorCourses.getString(2) != null)
            sync.setEndDate(getDate(cursorCourses.getString(2)));
        sync.setStatus(Status.valueOf(cursorCourses.getString(3)));
        return sync;
    }

}
