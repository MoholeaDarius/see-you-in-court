package com.example.seeyouincourt.model;

import com.example.seeyouincourt.model.constants.Status;

import java.time.LocalDateTime;
import java.util.Objects;

public class Sync extends AbstractEntity{

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private Status status;

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Sync sync = (Sync) o;
        return Objects.equals(startDate, sync.startDate) && Objects.equals(endDate, sync.endDate) && status == sync.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), startDate, endDate, status);
    }

    @Override
    public String toString() {
        return "Sync{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", status=" + status +
                '}';
    }

}
