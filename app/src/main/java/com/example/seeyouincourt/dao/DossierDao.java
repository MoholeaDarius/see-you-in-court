package com.example.seeyouincourt.dao;

import com.example.seeyouincourt.model.Dossier;

import java.util.List;
import java.util.Optional;

/**
 * data base interface, implements some methods form DAO
 */
public interface DossierDao {

    Optional<List<Dossier>> get();

    Optional<List<Dossier>> getLastUpdatedDossiers();

    void add(Dossier dossier);

    void update(Dossier dossier);

    void deleteAll();

    void delete(Dossier dossier);

}
