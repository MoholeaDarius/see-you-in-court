package com.example.seeyouincourt.ui.dossiers.tools;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.concurrent.ExecutionException;


/**
 * Provide data about device
 */
public class DeviceConfig {

    public static final double AVERAGE_TABLET_DIAGONAL = 6.5;

    public static double getScreenDiagonal(WindowManager manager) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(displayMetrics);
        float yInches = displayMetrics.heightPixels / displayMetrics.ydpi;
        float xInches = displayMetrics.widthPixels / displayMetrics.xdpi;
        return Math.sqrt(xInches * xInches + yInches * yInches);
    }

    public static boolean isInLandscapeMode(Resources resources) {
        int screenOrientation = resources.getConfiguration().orientation;
        return screenOrientation != Configuration.ORIENTATION_PORTRAIT;
    }

    public static boolean isOnline() {
        try {
            return new CheckNetworkConnection().execute().get();
        } catch (ExecutionException  | InterruptedException e) {
            return false;
        }
    }

    private static class CheckNetworkConnection extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                int timeoutMs = 2000;
                Socket sock = new Socket();
                SocketAddress socketAddress = new InetSocketAddress("8.8.8.8", 53);

                sock.connect(socketAddress, timeoutMs);
                sock.close();

                return true;
            } catch (IOException e) {
                return false;
            }
        }
    }

}
