package com.example.seeyouincourt.ui.meetings.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.model.CalendarMeeting;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.service.DossierService;
import com.example.seeyouincourt.ui.dossiers.DossierDetailsActivity;
import com.example.seeyouincourt.ui.dossiers.tools.SingletonDataContainer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is a adapter for Meetings items in calendar activity.
 */

public class MeetingAdapterForCalendar extends RecyclerView.Adapter<MeetingAdapterForCalendar.MeetingVH> {

    private List<CalendarMeeting> calendarMeetings;
    private DossierService service;

    public MeetingAdapterForCalendar(List<CalendarMeeting> calendarMeetings, Context context) {
        this.calendarMeetings = calendarMeetings;
        this.service = DossierService.getInstance(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public MeetingVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.meeting_item_for_calendar, parent, false);
        return new MeetingAdapterForCalendar.MeetingVH(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull MeetingVH holder, int position) {
        CalendarMeeting calendarMeeting = calendarMeetings.get(position);
        holder.hourTextView.setText(getDate(calendarMeeting.getDate()));
        holder.aliasTextView.setText(calendarMeeting.getAlias());
        holder.courtTextView.setText(calendarMeeting.getCourt().toString());
        holder.dossierNoTextView.setText(calendarMeeting.getDossierNo());
        if (calendarMeeting.isExpended()) {
            if (calendarMeeting.getComplete().equals("anyType{}")) {
                holder.completeNameTextView.setVisibility(View.GONE);
                holder.completeTextView.setVisibility(View.GONE);
            } else {
                holder.completeNameTextView.setVisibility(View.VISIBLE);
                holder.completeTextView.setVisibility(View.VISIBLE);
                holder.completeTextView.setText(calendarMeeting.getComplete());
            }
            if (calendarMeeting.getSolution().equals("anyType{}")) {
                holder.solutionNameTextView.setVisibility(View.GONE);
                holder.solutionTextView.setVisibility(View.GONE);
            } else {
                holder.solutionNameTextView.setVisibility(View.VISIBLE);
                holder.solutionTextView.setVisibility(View.VISIBLE);
                holder.solutionTextView.setText(calendarMeeting.getSolution());
            }
            if (calendarMeeting.getSolutionSummary().equals("anyType{}")) {
                holder.solutionSummaryNameTextView.setVisibility(View.GONE);
                holder.solutionSummaryTextView.setVisibility(View.GONE);
            } else {
                holder.solutionSummaryNameTextView.setVisibility(View.VISIBLE);
                holder.solutionSummaryTextView.setVisibility(View.VISIBLE);
                holder.solutionSummaryTextView.setText(calendarMeeting.getSolutionSummary());
            }
            holder.dossierDetailsButton.setOnClickListener(l ->{
                SingletonDataContainer.getInstance().setGoInDossiersList(false);
                Intent intent = new Intent(holder.itemView.getContext(), DossierDetailsActivity.class);
                intent.putExtra("dossiers", (ArrayList<Dossier>) service.getDossiersByNo(calendarMeeting.getDossierNo()));
                l.getContext().startActivity(intent);
            });
        }
        holder.expandableLayout.setVisibility(calendarMeeting.isExpended() ? View.VISIBLE : View.GONE);
    }

    private String getDate(LocalDateTime time) {
        String[] fullDate = time.toString().split("T");
        return fullDate[1];
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return calendarMeetings.size();
    }

    class MeetingVH extends RecyclerView.ViewHolder {

        private final LinearLayout expandableLayout;
        private final TextView hourTextView;
        private final TextView aliasTextView;
        private final TextView courtTextView;
        private final TextView dossierNoTextView;
        private final TextView completeTextView;
        private final TextView completeNameTextView;
        private final TextView solutionTextView;
        private final TextView solutionNameTextView;
        private final TextView solutionSummaryTextView;
        private final TextView solutionSummaryNameTextView;
        private final Button dossierDetailsButton;

        @RequiresApi(api = Build.VERSION_CODES.N)
        public MeetingVH(@NonNull final View itemView) {
            super(itemView);
            hourTextView = itemView.findViewById(R.id.hour_text_view);
            aliasTextView = itemView.findViewById(R.id.alias_text_view_for_cal);
            courtTextView = itemView.findViewById(R.id.court_text_view);
            dossierNoTextView = itemView.findViewById(R.id.dossier_no_text_view_for_cal);
            completeTextView = itemView.findViewById(R.id.complete_text_view_for_cal);
            completeNameTextView = itemView.findViewById(R.id.complete_name_text_view_for_cal);
            solutionTextView = itemView.findViewById(R.id.solution_text_view_for_cal);
            solutionNameTextView = itemView.findViewById(R.id.solution_name_text_view_for_cal);
            solutionSummaryTextView = itemView.findViewById(R.id.solution_summary_text_view_for_cal);
            solutionSummaryNameTextView = itemView.findViewById(R.id.solution_summary_name_text_view_for_cal);
            expandableLayout = itemView.findViewById(R.id.extended_layout_for_cal);
            LinearLayout detailsLayout = itemView.findViewById(R.id.details_layout_for_cal);
            dossierDetailsButton = itemView.findViewById(R.id.dossier_details_button);

            detailsLayout.setOnClickListener(view -> {
                CalendarMeeting calendarMeeting = calendarMeetings.get(getAdapterPosition());
                calendarMeeting.setExpended(!calendarMeeting.isExpended());
                notifyItemChanged(getAdapterPosition());
            });

            expandableLayout.setOnClickListener(view -> {
                CalendarMeeting calendarMeeting = calendarMeetings.get(getAdapterPosition());
                calendarMeeting.setExpended(!calendarMeeting.isExpended());
                notifyItemChanged(getAdapterPosition());
            });

        }
    }

}
