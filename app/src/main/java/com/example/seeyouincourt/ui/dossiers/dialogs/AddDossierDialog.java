package com.example.seeyouincourt.ui.dossiers.dialogs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.exception.*;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.service.DossierService;
import com.example.seeyouincourt.service.portal.DossierWebClient;
import com.example.seeyouincourt.service.portal.DossierWebClientImpl;
import com.example.seeyouincourt.ui.dossiers.constants.ToastMessages;
import com.example.seeyouincourt.ui.dossiers.tools.DeviceConfig;
import com.example.seeyouincourt.ui.dossiers.tools.ToastMaker;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

/**
 * Dialog fragment used to add a dossiers.
 * Use dossierNo and alias provided by the user.
 */

public class AddDossierDialog extends DialogFragment {

    private ImageView closeDialogIcon;
    private TextInputEditText dossierNoEditText;
    private EditText aliasEditText;
    private Button addDossierBtn;
    private DossierWebClient dossierWebClient;
    private List<Dossier> dossiers;
    private CallBack callBack;
    private DossierService service;

    @SuppressLint("NewApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View dialogView = inflater.inflate(R.layout.add_dossier_dialog, container, false);
        initViews(dialogView);
        service = DossierService.getInstance(requireContext());

        closeDialogIcon.setOnClickListener(e -> Objects.requireNonNull(getDialog()).dismiss());
        addDossierBtn.setOnClickListener(e -> {
            if (!DeviceConfig.isOnline()) {
                ToastMaker.displayErrorToast(requireActivity(), requireContext(), ToastMessages.NO_INTERNET_CONNECTION.getValue());
            } else {
                if ((Objects.requireNonNull(dossierNoEditText.getText()).toString()).equals("")) {
                    ToastMaker.displayWarningToast(requireActivity(), requireContext(), ToastMessages.DOSSIER_FIELD_IS_EMPTY.getValue());
                } else if (!dossierNoEditText.getText().toString().matches("[0-9]+/[0-9]+/.+")) {
                    ToastMaker.displayErrorToast(requireActivity(), requireContext(), ToastMessages.INVALID_DOSSIER_NO.getValue());
                    dossierNoEditText.getText().clear();
                } else if (Objects.requireNonNull(getDossiersByDossierNoFromPortal()).isEmpty()) {
                    ToastMaker.displayErrorToast(requireActivity(), requireContext(), ToastMessages.INVALID_DOSSIER_NO.getValue());
                    dossierNoEditText.getText().clear();
                } else if (checkIfDossierAlreadyExists(dossierNoEditText.getText().toString())) {
                    ToastMaker.displayErrorToast(requireActivity(), requireContext(), ToastMessages.DOSSIER_ALREADY_EXISTS.getValue());
                    dossierNoEditText.getText().clear();
                } else {
                    dossiers = getDossiersByDossierNoFromPortal();
                    dossiers.sort((dossier1, dossier2) -> dossier2.getModificationDate().compareTo(dossier1.getModificationDate()));
                    for (Dossier dossier : dossiers) {
                        dossier.setName(aliasEditText.getText().toString());
                    }
                    callBack.onDossiersSent(dossiers);
                    Objects.requireNonNull(getDialog()).dismiss();
                    ToastMaker.displaySuccessToast(requireActivity(), requireContext(), ToastMessages.DOSSIER_ADDED_SUCCESSFULLY.getValue());
                }
            }
        });

        return dialogView;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private boolean checkIfDossierAlreadyExists(String dossierNo) {
        if (service.getDossiers().isPresent())
            for (Dossier dossier : service.getDossiers().get())
                if (dossier.getNumber().equals(dossierNo))
                    return true;
        return false;
    }

    private List<Dossier> getDossiersByDossierNoFromPortal() {
        dossierWebClient = new DossierWebClientImpl();
        try {
            return new CallSearchDossier().execute().get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initViews(View view) {
        closeDialogIcon = view.findViewById(R.id.close_dialog);
        dossierNoEditText = view.findViewById(R.id.dossier_no_dialog_input_text);
        aliasEditText = view.findViewById(R.id.alias_dialog_input_text);
        addDossierBtn = view.findViewById(R.id.add_dossier_dialog_btn);
    }

    private class CallSearchDossier extends AsyncTask<Void, Void, List<Dossier>> {

        @Override
        protected List<Dossier> doInBackground(Void... voids) {
            try {
                return dossierWebClient.getDossiers(Objects.requireNonNull(dossierNoEditText.getText()).toString());
            } catch (JustPortalException e) {
                return new ArrayList<>();
            }
        }

    }

    @Override
    public void onAttach(@NonNull Context context) {
        callBack = (CallBack) getTargetFragment();
        super.onAttach(context);
    }

}
