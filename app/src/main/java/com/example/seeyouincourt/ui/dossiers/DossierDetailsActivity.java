package com.example.seeyouincourt.ui.dossiers;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import com.example.seeyouincourt.MainActivity;
import com.example.seeyouincourt.R;
import com.example.seeyouincourt.dao.DossierDaoImpl;
import com.example.seeyouincourt.dao.MeetingDaoImpl;
import com.example.seeyouincourt.dao.SyncDaoImpl;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.service.DossierService;
import com.example.seeyouincourt.ui.dossiers.adapters.DossierDetailsFragmentAdapter;
import com.example.seeyouincourt.ui.dossiers.constants.ToastMessages;
import com.example.seeyouincourt.ui.dossiers.tools.DeviceConfig;
import com.example.seeyouincourt.ui.dossiers.tools.ToastMaker;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

/**
 * This is an activity for dossier details.
 * Contains a view pager.
 */
public class DossierDetailsActivity extends AppCompatActivity {

    private TabLayout tabLayout;

    private ViewPager2 viewPager;

    private EditText aliasEditText;

    private DossierService dossierService;

    private double screenDiagonalInches;
    private List<Dossier> dossiers;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dossier_details_activity);
        screenDiagonalInches = DeviceConfig.getScreenDiagonal(getWindowManager());
        initViews();
        this.dossierService = DossierService.getInstance(DossierDetailsActivity.this);

        this.dossiers = (List<Dossier>) getIntent().getSerializableExtra("dossiers");
        initViewPager(dossiers);
        setupToolbar(dossiers.get(0).getNumber(), dossiers.get(0).getName());
    }

    private void setupToolbar(String title, String alias) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(e -> onBackPressed());
        toolbar.setTitle(title);

        aliasEditText = findViewById(R.id.alias_edit_text);
        aliasEditText.setText(alias);
        aliasEditText.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEND) {
                // Update in DB
                for (Dossier dossier : dossiers) {
                    dossier.setName(aliasEditText.getText().toString());
                    dossierService.updateDossier(dossier);
                }

                //Defocus & display Toast.
                Handler handler = new Handler();
                handler.postDelayed(() -> {
                    ToastMaker.displaySuccessToast(this, getBaseContext(), ToastMessages.ALIAS_CHANGED_SUCCESSFULLY.getValue());
                    aliasEditText.clearFocus();
                }, 300);
                return false;
            }
            return true;
        });

    }

    private void initViews() {
        tabLayout = findViewById(R.id.dossier_details_tab_layout);
        viewPager = findViewById(R.id.dossier_details_view_pager);
    }

    private void initViewPager(List<Dossier> dossiers) {
        viewPager.setAdapter(getFragmentAdapter(dossiers));
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });

        //Tab Layout
        for (Dossier dossier : dossiers) {
            tabLayout.addTab(tabLayout.newTab().setText(dossier.getCourt().getValue()));
        }
        tabLayout.addOnTabSelectedListener(createOnTabSelectedListener());
    }

    private DossierDetailsFragmentAdapter getFragmentAdapter(List<Dossier> dossiers) {
        return new DossierDetailsFragmentAdapter(getSupportFragmentManager(), getLifecycle(), dossiers);
    }

    private TabLayout.BaseOnTabSelectedListener createOnTabSelectedListener() {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (screenDiagonalInches > DeviceConfig.AVERAGE_TABLET_DIAGONAL && newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            onBackPressed();
        }
    }

}
