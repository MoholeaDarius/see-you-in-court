package com.example.seeyouincourt.model.constants;

public enum Status {

    FAIL("Fail"),
    SUCCESS("Success");

    private final String value;

    Status(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
