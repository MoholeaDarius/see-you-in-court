package com.example.seeyouincourt.ui.meetings;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.model.CalendarMeeting;
import com.example.seeyouincourt.service.DossierService;
import com.example.seeyouincourt.ui.meetings.adapter.MeetingAdapterForCalendar;
import com.example.seeyouincourt.ui.meetings.adapter.MeetingAdapterForMeetingList;
import com.example.seeyouincourt.ui.meetings.constants.DayOfWeek;
import com.example.seeyouincourt.util.DateUtil;

import java.time.LocalDate;
import java.util.List;

public class MeetingListFragment extends Fragment {

    private TextView todayTextView;
    private TextView todayDateTextView;
    private TextView day2TextView;
    private TextView day2DateTextView;
    private TextView day3TextView;
    private TextView day3DateTextView;
    private TextView day4TextView;
    private TextView day4DateTextView;
    private TextView day5TextView;
    private TextView day5DateTextView;
    private RecyclerView todayRecyclerView;
    private RecyclerView day2RecyclerView;
    private RecyclerView day3RecyclerView;
    private RecyclerView day4RecyclerView;
    private RecyclerView day5RecyclerView;
    private RecyclerView futureMeetingsRecyclerView;

    private LinearLayout todayLinerLayout;
    private LinearLayout day2LinerLayout;
    private LinearLayout day3LinerLayout;
    private LinearLayout day4LinerLayout;
    private LinearLayout day5LinerLayout;
    private LinearLayout futureLinerLayout;

    private DossierService service;

    private List<LocalDate> future5Dates;
    private List<CalendarMeeting> futureSortedCalendarMeetings;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.meetings_list_fragment, container, false);

        initViews(view);

        this.service = DossierService.getInstance(requireContext());
        futureSortedCalendarMeetings = service.getFutureSortedCalendarMeetings();
        future5Dates = DateUtil.getNextWorkingDates(5);

        setWorkingDayComponents(todayTextView, todayDateTextView, 0, todayRecyclerView, todayLinerLayout);
        setWorkingDayComponents(day2TextView, day2DateTextView, 1, day2RecyclerView, day2LinerLayout);
        setWorkingDayComponents(day3TextView, day3DateTextView, 2, day3RecyclerView, day3LinerLayout);
        setWorkingDayComponents(day4TextView, day4DateTextView, 3, day4RecyclerView, day4LinerLayout);
        setWorkingDayComponents(day5TextView, day5DateTextView, 4, day5RecyclerView, day5LinerLayout);
        setFutureComponent();

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setWorkingDayComponents(TextView workingDayTextView, TextView workingDateTextView, int workingDateIndex, RecyclerView recyclerView, LinearLayout workingDayLayout) {
        if ((DateUtil.getCalendarMeetingsByDate(futureSortedCalendarMeetings, future5Dates.get(workingDateIndex)).isEmpty())) {
            workingDayLayout.setVisibility(View.GONE);
        } else {
            if (future5Dates.get(workingDateIndex).equals(LocalDate.now())) {
                workingDayTextView.setText(DateUtil.TODAY);
            } else if (future5Dates.get(workingDateIndex).equals(LocalDate.now().plusDays(1))) {
                workingDayTextView.setText(DateUtil.TOMORROW);
            } else {
                workingDayTextView.setText(DayOfWeek.findValue(future5Dates.get(workingDateIndex).getDayOfWeek().toString()) + "- ");
                workingDateTextView.setText(future5Dates.get(workingDateIndex).format(DateUtil.dateFormatter));
            }
            setUpRecyclerView(recyclerView, new MeetingAdapterForCalendar(DateUtil.getCalendarMeetingsByDate(futureSortedCalendarMeetings, future5Dates.get(workingDateIndex)), requireContext()));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setFutureComponent() {
        if (service.getFutureCalendarMeetings().isEmpty()) {
            futureLinerLayout.setVisibility(View.GONE);
        } else {
            setUpRecyclerView(futureMeetingsRecyclerView, new MeetingAdapterForMeetingList(service.getFutureCalendarMeetings(), requireContext()));
        }
    }

    private void setUpRecyclerView(RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    private void initViews(View view) {
        todayLinerLayout = view.findViewById(R.id.today_layout);
        todayTextView = view.findViewById(R.id.today_text_view);
        todayDateTextView = view.findViewById(R.id.today_date);
        todayRecyclerView = view.findViewById(R.id.today_meetings_recycler_view);

        day2LinerLayout = view.findViewById(R.id.day2_layout);
        day2TextView = view.findViewById(R.id.day2_text_view);
        day2DateTextView = view.findViewById(R.id.day2_date);
        day2RecyclerView = view.findViewById(R.id.day2_meetings_recycler_view);

        day3LinerLayout = view.findViewById(R.id.day3_layout);
        day3TextView = view.findViewById(R.id.day3_text_view);
        day3DateTextView = view.findViewById(R.id.day3_date);
        day3RecyclerView = view.findViewById(R.id.day3_meetings_recycler_view);

        day4LinerLayout = view.findViewById(R.id.day4_layout);
        day4TextView = view.findViewById(R.id.day4_text_view);
        day4DateTextView = view.findViewById(R.id.day4_date);
        day4RecyclerView = view.findViewById(R.id.day4_meetings_recycler_view);

        day5LinerLayout = view.findViewById(R.id.day5_layout);
        day5TextView = view.findViewById(R.id.day5_text_view);
        day5DateTextView = view.findViewById(R.id.day5_date);
        day5RecyclerView = view.findViewById(R.id.day5_meetings_recycler_view);

        futureLinerLayout = view.findViewById(R.id.future_layout);
        futureMeetingsRecyclerView = view.findViewById(R.id.all_future_meetings_recycler_view);
    }

}
