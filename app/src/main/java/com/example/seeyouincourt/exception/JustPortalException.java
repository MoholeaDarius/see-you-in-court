package com.example.seeyouincourt.exception;

public class JustPortalException extends Exception {

    public JustPortalException() {
        super();
    }

    public JustPortalException(String message) {
        super(message);
    }

}
