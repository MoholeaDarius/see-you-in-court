package com.example.seeyouincourt.ui.devlopment.security;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.ui.devlopment.DevFragment;
import com.example.seeyouincourt.ui.dossiers.tools.ToastMaker;

public class DevSecurityFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dev_login_fragment, container, false);

        EditText passwordBox = view.findViewById(R.id.passwordBox);
        Button verifyPasswordButton = view.findViewById(R.id.verify_password_btn);

        verifyPasswordButton.setOnClickListener(click -> {
            if (passwordBox.getText().toString().equals("team1o1")) {
                getParentFragmentManager()
                        .beginTransaction()
                        .replace(R.id.main_fragment_container, new DevFragment())
                        .commit();
                InputMethodManager systemService = (InputMethodManager) requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                systemService.hideSoftInputFromWindow(view.getWindowToken(), 0);
            } else {
                ToastMaker.displayErrorToast(requireActivity(), requireContext(), "Invalid password!");
            }
        });

        return view;
    }
}
