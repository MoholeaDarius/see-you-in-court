package com.example.seeyouincourt.model;

import com.example.seeyouincourt.model.constants.Court;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Pojo for meeting framgent.
 */
public class CalendarMeeting {

    private LocalDateTime date;
    private Court court;
    private String dossierNo;
    private String alias;
    private String complete;
    private String solution;
    private String solutionSummary;
    private long dossierId;
    private boolean expended;

    public CalendarMeeting(LocalDateTime date, Court court, String dossierNo, String alias, String complete, String solution, String solutionSummary, long dossierId, boolean expended) {
        this.date = date;
        this.court = court;
        this.dossierNo = dossierNo;
        this.alias = alias;
        this.complete = complete;
        this.solution = solution;
        this.solutionSummary = solutionSummary;
        this.dossierId = dossierId;
        this.expended = expended;
    }

    public boolean isExpended() {
        return expended;
    }

    public void setExpended(boolean expended) {
        this.expended = expended;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Court getCourt() {
        return court;
    }

    public void setCourt(Court court) {
        this.court = court;
    }

    public String getDossierNo() {
        return dossierNo;
    }

    public void setDossierNo(String dossierNo) {
        this.dossierNo = dossierNo;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getComplete() {
        return complete;
    }

    public void setComplete(String complete) {
        this.complete = complete;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getSolutionSummary() {
        return solutionSummary;
    }

    public void setSolutionSummary(String solutionSummary) {
        this.solutionSummary = solutionSummary;
    }

    public long getDossierId() {
        return dossierId;
    }

    public void setDossierId(long dossierId) {
        this.dossierId = dossierId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalendarMeeting that = (CalendarMeeting) o;
        return dossierId == that.dossierId && expended == that.expended && Objects.equals(date, that.date) && court == that.court && Objects.equals(dossierNo, that.dossierNo) && Objects.equals(alias, that.alias) && Objects.equals(complete, that.complete) && Objects.equals(solution, that.solution) && Objects.equals(solutionSummary, that.solutionSummary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, court, dossierNo, alias, complete, solution, solutionSummary, dossierId, expended);
    }

    @Override
    public String toString() {
        return "CalendarMeeting{" +
                "date=" + date +
                ", court=" + court +
                ", dossierNo='" + dossierNo + '\'' +
                ", alias='" + alias + '\'' +
                ", complete='" + complete + '\'' +
                ", solution='" + solution + '\'' +
                ", solutionSummary='" + solutionSummary + '\'' +
                ", dossierId=" + dossierId +
                ", expended=" + expended +
                '}';
    }
}
