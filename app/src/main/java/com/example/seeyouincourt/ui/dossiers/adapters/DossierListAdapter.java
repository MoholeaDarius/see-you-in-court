package com.example.seeyouincourt.ui.dossiers.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.ui.dossiers.tools.FilterTool;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This class manage an item in the dossiers list.
 */

public class DossierListAdapter extends ArrayAdapter implements Filterable {

    private final Context mContext;
    private final int mResource;

    private int longPressedItemPos;
    private boolean isActionMode;
    private ActionMode contextualActionMode;

    private List<Dossier> dossiersToDelete;
    private final List<Dossier> allDossiers;
    private final List<Dossier> allDossierCpy;

    public DossierListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Dossier> allDossiers) {
        super(context, resource, allDossiers);
        mContext = context;
        mResource = resource;
        this.allDossiers = allDossiers;

        allDossierCpy = new ArrayList<>();
        allDossierCpy.addAll(allDossiers);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return filter;
    }

    Filter filter = new Filter() {

        ///this  method run on a background thread that is create automatically
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected FilterResults performFiltering(CharSequence input) {
            List<Dossier> filteredDossiers = new ArrayList<>();

            if (input.toString().isEmpty()) {
                filteredDossiers.addAll(allDossierCpy);
            } else {
                filteredDossiers.addAll(FilterTool.filter(allDossierCpy, input.toString()));
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredDossiers;
            return filterResults;
        }

        ///this run on UI thread, publish results
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            allDossiers.clear();
            allDossiers.addAll((Collection<? extends Dossier>) filterResults.values);
            notifyDataSetChanged();
        }
    };

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Dossier dossier = (Dossier) getItem(position);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView numberTextView = convertView.findViewById(R.id.dossier_number_text_view);
        numberTextView.setText(dossier.getNumber());

        TextView aliasTextView = convertView.findViewById(R.id.alias_text_view);
        aliasTextView.setText(dossier.getName());

        TextView judgeInstanceTextView = convertView.findViewById(R.id.judge_instance_text_view);
        judgeInstanceTextView.setText(dossier.getCourt().getValue());

        CheckBox checkBox = convertView.findViewById(R.id.contextual_checkbox);

        if (isActionMode) {

            if (position == longPressedItemPos) setLongPressedItemChecked(dossier, checkBox);

            setOnCheckedChangeListener(dossier, checkBox);

            checkBox.setVisibility(View.VISIBLE);

        } else checkBox.setVisibility(View.GONE);


        return convertView;
    }

    private void setLongPressedItemChecked(Dossier dossier, CheckBox checkBox) {
        checkBox.setChecked(true);
        if (dossiersToDelete.contains(dossier)) {
            dossiersToDelete.remove(dossier);
        } else {
            dossiersToDelete.add(dossier);
        }

        contextualActionMode.setTitle(dossiersToDelete.size() == 1 ? "1 item selected" : dossiersToDelete.size() + " items selected");
    }

    private void setOnCheckedChangeListener(Dossier dossier, CheckBox checkBox) {
        checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (dossiersToDelete.contains(dossier)) {
                dossiersToDelete.remove(dossier);
            } else {
                dossiersToDelete.add(dossier);
            }

            contextualActionMode.setTitle(dossiersToDelete.size() == 1 ? "1 item selected" : dossiersToDelete.size() + " items selected");
        });
    }

    public void setLongPressedItemPos(int longPressedItemPos) {
        this.longPressedItemPos = longPressedItemPos;
    }

    public void setActionMode(boolean actionMode) {
        isActionMode = actionMode;
    }

    public List<Dossier> getDossiersToDelete() {
        return dossiersToDelete;
    }

    public void setDossiersToDelete(List<Dossier> dossiersToDelete) {
        this.dossiersToDelete = dossiersToDelete;
    }

    public void setContextualActionMode(ActionMode contextualActionMode) {
        this.contextualActionMode = contextualActionMode;
    }

}
