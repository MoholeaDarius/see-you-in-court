package com.example.seeyouincourt.ui.Sync;

import static com.example.seeyouincourt.util.DialogBuilder.buildLoadingDialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.model.constants.Status;
import com.example.seeyouincourt.model.Sync;
import com.example.seeyouincourt.service.SyncService;
import com.example.seeyouincourt.ui.dossiers.tools.ToastMaker;
import com.example.seeyouincourt.util.DateUtil;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class SyncFragment extends Fragment {

    private Button syncButton;

    private SyncService syncService;

    private TextView endDateTextView;
    private TextView endHourTextView;

    public SyncFragment(Context context) {
        syncService = SyncService.getInstance(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sync_fragment, container, false);
        initViews(view);

        syncButton.setOnClickListener(e -> {
            ProgressDialog progressDialog = buildLoadingDialog(requireContext());
            progressDialog.show();
            progressDialog.setContentView(R.layout.loading_dialog);

            ///close loading dialog and display toast message after 2 seconds
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                syncService.syncDossiers();
                progressDialog.dismiss();
                endDateTextView.setText(LocalDate.now().format(DateUtil.dateFormatter));
                endHourTextView.setText(LocalTime.now().format(DateUtil.hourFormatter));
                RelativeLayout syncFailedLayout = requireActivity().findViewById(R.id.sync_failed_main);
                if (syncService.getLastSync().getStatus().equals(Status.SUCCESS)) {
                    syncFailedLayout.setVisibility(View.GONE);
                } else {
                    syncFailedLayout.setVisibility(View.VISIBLE);
                }
                ToastMaker.displaySuccessToast(getActivity(), getContext(), "Sync done!");
            }, 2000);
        });

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initViews(View view) {
        syncButton = view.findViewById(R.id.sync_button);
        endDateTextView = view.findViewById(R.id.sync_date_txt);
        endHourTextView = view.findViewById(R.id.sync_hour_txt);

        Sync lastValidSync = getLastValidSync();
        if (lastValidSync != null) {
            endDateTextView.setText(lastValidSync.getEndDate().format(DateUtil.dateFormatter));
            endHourTextView.setText(lastValidSync.getEndDate().toLocalTime().format(DateUtil.hourFormatter));
        } else {
            endDateTextView.setText("");
            endHourTextView.setText("");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private Sync getLastValidSync() {
        List<Sync> syncList = syncService.getSyncList().orElse(new ArrayList<>());
        for (int i = syncList.size() - 1; i >= 0; i--) {
            if (syncList.get(i).getStatus().equals(Status.SUCCESS)) return syncList.get(i);
        }
        return null;
    }

}