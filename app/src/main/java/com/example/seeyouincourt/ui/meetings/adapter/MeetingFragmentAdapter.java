package com.example.seeyouincourt.ui.meetings.adapter;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.seeyouincourt.ui.meetings.*;

/**
 * Adapter used by meetings fragment.
 * Manage related tabs.
 */
public class MeetingFragmentAdapter extends FragmentStateAdapter {


    public MeetingFragmentAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position == 0) {
            return new MeetingCalendarFragment();
        }
        return new MeetingListFragment();
    }

    @Override
    public int getItemCount() {
        return 2;
    }

}
