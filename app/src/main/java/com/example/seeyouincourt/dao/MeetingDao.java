package com.example.seeyouincourt.dao;

import com.example.seeyouincourt.model.Meeting;

import java.util.List;
import java.util.Optional;

public interface MeetingDao {

    void add(List<Meeting> meetings, long dossierId);

    Optional<List<Meeting>> get();

    Optional<List<Meeting>> getByDossierId(int dossierId);

    void deleteAll();

}
