package com.example.seeyouincourt.dao;

import com.example.seeyouincourt.model.Sync;

import java.util.List;
import java.util.Optional;

public interface SyncDao {

    Optional<List<Sync>> get();

    Sync getLast();

    long add(Sync sync);

    /**
     * Update entity based on id. If id is null, Illegal State Exception will be thrown.
     */
    void update(Sync sync);

    void deleteAll();

}
