package com.example.seeyouincourt.ui.dossiers;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.ui.dossiers.adapters.*;
import com.example.seeyouincourt.util.DateUtil;

import java.time.format.DateTimeFormatter;

/**
 * This fragment fill a page from view pager.
 * The information displayed depending on the court.
 */
@RequiresApi(api = Build.VERSION_CODES.O)
public class DossierDetailsFragment extends Fragment {

    private RecyclerView recyclerView;
    private TextView errorTextView;
    private TextView dossierNoTextView;
    private TextView courtTextView;
    private TextView registrationDateTextView;
    private TextView modificationDateTextView;
    private TextView departmentTextView;
    private TextView caseCategoryTextView;
    private TextView objectTextView;
    private TextView processStageTextView;
    private Dossier dossier;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dossier_details_fragment, container, false);
        initViews(view);

        if (dossier.getMeetings() != null) {
            MeetingAdapter meetingAdapter = new MeetingAdapter(dossier.getMeetings());
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(meetingAdapter);
        } else {
            errorTextView.setText("      CE faci bili vezi ca nu exista sedinte");
        }

        return view;
    }


    public void setDossier(Dossier dossier) {
        this.dossier = dossier;
    }

    private void initViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        errorTextView = view.findViewById(R.id.nu_exista_sedinte_text_view);

        dossierNoTextView = view.findViewById(R.id.dossier_no_text_view);
        dossierNoTextView.setText(dossier.getNumber());

        courtTextView = view.findViewById(R.id.court_text_view);
        courtTextView.setText(dossier.getCourt().getValue());

        registrationDateTextView = view.findViewById(R.id.registration_date_text_view);
        registrationDateTextView.setText(dossier.getDate().format(DateUtil.dateFormatter));

        modificationDateTextView = view.findViewById(R.id.modification_date_text_view);
        modificationDateTextView.setText(dossier.getModificationDate().format(DateUtil.dateFormatter));

        departmentTextView = view.findViewById(R.id.department_text_view);
        departmentTextView.setText(dossier.getDepartment());

        caseCategoryTextView = view.findViewById(R.id.case_category_text_view);
        caseCategoryTextView.setText(dossier.getCaseCategoryName());

        objectTextView = view.findViewById(R.id.object_text_view);
        objectTextView.setText(dossier.getObject());

        processStageTextView = view.findViewById(R.id.process_stage_text_view);
        processStageTextView.setText(dossier.getProcessStageName());
    }

}
