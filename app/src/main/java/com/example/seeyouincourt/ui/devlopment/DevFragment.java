package com.example.seeyouincourt.ui.devlopment;

import static com.example.seeyouincourt.ui.dossiers.constants.DummyDataProvider.buildALotDossierNumbers;
import static com.example.seeyouincourt.ui.dossiers.constants.DummyDataProvider.buildSomeDossiersNumbers;
import static com.example.seeyouincourt.util.DialogBuilder.buildLoadingDialog;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.model.Sync;
import com.example.seeyouincourt.model.constants.Status;
import com.example.seeyouincourt.service.DossierService;
import com.example.seeyouincourt.service.SyncService;
import com.example.seeyouincourt.service.portal.DossierWebClientImpl;
import com.example.seeyouincourt.ui.devlopment.adapters.SyncListAdapter;
import com.example.seeyouincourt.ui.dossiers.tools.ToastMaker;
import com.example.seeyouincourt.exception.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class DevFragment extends Fragment {

    private DossierService service;
    private SyncService syncService;
    private DossierWebClientImpl dossierWebClient;

    private List<Sync> syncs;

    private Button deleteAllMeetingsButton;
    private Button deleteAllDossiersButton;
    private Button deleteAllSyncsButton;
    private Button syncNowButton;
    private Button addAFewDefaultDossiersButton;
    private Button addALittleMoreDefaultDossiersButton;

    private SyncListAdapter syncsListAdapter;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dev_fragment, container, false);
        service = DossierService.getInstance(requireContext());
        syncService = SyncService.getInstance(requireContext());
        dossierWebClient = new DossierWebClientImpl();
        syncs = syncService.getSyncList().orElse(new ArrayList<>());

        initViews(view);

        deleteAllMeetingsButton.setOnClickListener(click -> {
            service.deleteAllMeetings();
            ToastMaker.displaySuccessToast(requireActivity(), requireContext(), "Meetings deleted!");
        });

        syncNowButton.setOnClickListener(click -> {
            ProgressDialog progressDialog = buildLoadingDialog(requireContext());
            progressDialog.show();
            progressDialog.setContentView(R.layout.loading_dialog);

            ///close loading dialog and display toast message after 2 seconds
            Handler handler = new Handler();
            handler.postDelayed(() -> {
                syncService.syncDossiers();
                progressDialog.dismiss();
                ToastMaker.displaySuccessToast(getActivity(), getContext(), "Sync done!");
                syncs.clear();
                syncs.addAll(syncService.getSyncList().orElse(new ArrayList<>()));

                RelativeLayout syncFailedLayout = requireActivity().findViewById(R.id.sync_failed_main);
                if (syncService.getLastSync().getStatus().equals(Status.SUCCESS)) {
                    syncFailedLayout.setVisibility(View.GONE);
                } else {
                    syncFailedLayout.setVisibility(View.VISIBLE);
                }

                Collections.reverse(syncs);
                syncsListAdapter.notifyDataSetChanged();
            }, 2000);

        });

        addAFewDefaultDossiersButton.setOnClickListener(click -> {
            if (!service.getDossiers().isPresent() || service.getDossiers().get().isEmpty()) {
                List<String> dossierNumbers = buildSomeDossiersNumbers();
                dossierNumbers.forEach(dossierNumber -> {
                    try {
                        service.addNewDossiers(new CallSearchDossier(dossierNumber).execute().get());
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }
                });
                ToastMaker.displaySuccessToast(requireActivity(), requireContext(), "Dossiers added!");
            } else {
                ToastMaker.displayErrorToast(requireActivity(), requireContext(), "Dossiers already exist!");
            }
        });

        addALittleMoreDefaultDossiersButton.setOnClickListener(click -> {
            if (!service.getDossiers().isPresent() || service.getDossiers().get().isEmpty()) {
                List<String> dossierNumbers = buildALotDossierNumbers();
                dossierNumbers.forEach(dossierNumber -> {
                    try {
                        service.addNewDossiers(new CallSearchDossier(dossierNumber).execute().get());
                    } catch (ExecutionException | InterruptedException e) {
                        e.printStackTrace();
                    }
                });
                ToastMaker.displaySuccessToast(requireActivity(), requireContext(), "Dossiers added!");
            } else {
                ToastMaker.displayErrorToast(requireActivity(), requireContext(), "Dossiers already exist!");
            }
        });


        deleteAllDossiersButton.setOnClickListener(click -> {
            service.deleteAllDossiers();
            ToastMaker.displaySuccessToast(requireActivity(), requireContext(), "All dossiers deleted!");
        });

        deleteAllSyncsButton.setOnClickListener(click -> {
            syncService.deleteAllSyncs();
            syncs.clear();
            syncsListAdapter.notifyDataSetChanged();
            ToastMaker.displaySuccessToast(requireActivity(), requireContext(), "All syncs deleted!");
        });

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initViews(View view) {
        deleteAllMeetingsButton = view.findViewById(R.id.delete_meetings_btn_dev);
        deleteAllDossiersButton = view.findViewById(R.id.delete_dossiers_btn_dev);
        deleteAllSyncsButton = view.findViewById(R.id.delete_syncs_btn_dev);
        syncNowButton = view.findViewById(R.id.sync_button_dev);
        addAFewDefaultDossiersButton = view.findViewById(R.id.add_dossiers_btn_dev);
        addALittleMoreDefaultDossiersButton = view.findViewById(R.id.add_more_dossiers_btn_dev);

        ListView syncsListView = view.findViewById(R.id.syncs_list_dev);
        Collections.reverse(syncs);
        syncsListAdapter = new SyncListAdapter(requireContext(), R.layout.sync_item, (ArrayList<Sync>) syncs);
        syncsListView.setAdapter(syncsListAdapter);
    }

    private class CallSearchDossier extends AsyncTask<Void, Void, List<Dossier>> {

        private final String dossierNo;

        public CallSearchDossier(String dossierNo) {
            this.dossierNo = dossierNo;
        }

        @Override
        protected List<Dossier> doInBackground(Void... voids) {
            try {
                return dossierWebClient.getDossiers(dossierNo);
            } catch (JustPortalException e) {
                return new ArrayList<>();
            }
        }

    }

}
