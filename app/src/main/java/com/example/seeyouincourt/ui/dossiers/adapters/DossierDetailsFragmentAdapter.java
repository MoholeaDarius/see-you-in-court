package com.example.seeyouincourt.ui.dossiers.adapters;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.ui.dossiers.*;

import java.util.List;

/**
 * Adapter used by dossier details view pager.
 * Manage related tabs.
 */
public class DossierDetailsFragmentAdapter extends FragmentStateAdapter {

    private List<Dossier> dossiers;

    public DossierDetailsFragmentAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle, List<Dossier> dossiers) {
        super(fragmentManager, lifecycle);
        this.dossiers = dossiers;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        DossierDetailsFragment dossierDetailsFragment = new DossierDetailsFragment();
        dossierDetailsFragment.setDossier(dossiers.get(position));
        return dossierDetailsFragment;
    }

    @Override
    public int getItemCount() {
        return dossiers.size();
    }

}
