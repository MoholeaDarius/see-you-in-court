package com.example.seeyouincourt.ui.about_us;

import static androidx.core.content.ContextCompat.getSystemService;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.ui.dossiers.tools.ToastMaker;

/**
 * This class is used to manage about us fragment.
 */

public class AboutUsFragment extends Fragment {

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_us_fragment, container, false);

        setUpGadaleanLinks(view);
        setUpMoholeaLinks(view);
        return view;
    }

    private void setUpGadaleanLinks(View view) {
        TextView gadaMailTxt = view.findViewById(R.id.gada_mail_txt);
        gadaMailTxt.setPaintFlags(gadaMailTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        setMailToClipboard(gadaMailTxt);

        TextView gadaBitBucketTxt = view.findViewById(R.id.gada_bitbucket_txt);
        gadaBitBucketTxt.setPaintFlags(gadaBitBucketTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        openWebPageByUrl(gadaBitBucketTxt, "https://bitbucket.org/gadalean_emanuel/");

        TextView gadaLinkedInTxt = view.findViewById(R.id.gada_linked_in_txt);
        gadaLinkedInTxt.setPaintFlags(gadaLinkedInTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        openWebPageByUrl(gadaLinkedInTxt, "https://ro.linkedin.com/in/emanuel-gadalean/");

        TextView gadaFacebookTxt = view.findViewById(R.id.gada_facebook_txt);
        gadaFacebookTxt.setPaintFlags(gadaFacebookTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        openWebPageByUrl(gadaFacebookTxt, "https://m.facebook.com/profile.php?id=100006428020039");

        TextView gadaInstaTxt = view.findViewById(R.id.gada_instagram_txt);
        gadaInstaTxt.setPaintFlags(gadaInstaTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        openWebPageByUrl(gadaInstaTxt, "https://www.instagram.com/gadaleanemi/");
    }

    private void setUpMoholeaLinks(View view) {
        TextView mohoMailTxt = view.findViewById(R.id.moho_email_txt);
        mohoMailTxt.setPaintFlags(mohoMailTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        setMailToClipboard(mohoMailTxt);

        TextView mohoBitBucketTxt = view.findViewById(R.id.moho_bitbucket_txt);
        mohoBitBucketTxt.setPaintFlags(mohoBitBucketTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        openWebPageByUrl(mohoBitBucketTxt, "https://bitbucket.org/MoholeaDarius/");

        TextView mohoLinkedInTxt = view.findViewById(R.id.moho_linkedin_txt);
        mohoLinkedInTxt.setPaintFlags(mohoLinkedInTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        openWebPageByUrl(mohoLinkedInTxt, "https://ro.linkedin.com/in/darius-moholea-095920221/");

        TextView mohoFacebookTxt = view.findViewById(R.id.moho_facebook_txt);
        mohoFacebookTxt.setPaintFlags(mohoFacebookTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        openWebPageByUrl(mohoFacebookTxt, "https://www.facebook.com/profile.php?id=100013438393947");

        TextView mohoInstagramTxt = view.findViewById(R.id.moho_instagram_txt);
        mohoInstagramTxt.setPaintFlags(mohoInstagramTxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        openWebPageByUrl(mohoInstagramTxt, "https://www.instagram.com/moholeadarius/");
    }

    private void setMailToClipboard(TextView text) {
        text.setOnClickListener(click -> {
            ClipboardManager clipboard = (ClipboardManager) requireContext().getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Copied txt", text.getText());
            clipboard.setPrimaryClip(clip);
            ToastMaker.displaySuccessToast(requireActivity(), requireContext(), "Copy to clipboard!");
        });
    }

    void openWebPageByUrl(TextView textView, String url){
        textView.setOnClickListener(click -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        });
    }

}
