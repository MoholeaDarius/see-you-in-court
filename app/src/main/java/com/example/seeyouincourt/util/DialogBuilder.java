package com.example.seeyouincourt.util;

import android.app.ProgressDialog;
import android.content.Context;

public class DialogBuilder {

    public static ProgressDialog buildLoadingDialog(Context context)  {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        return progressDialog;
    }

}
