package com.example.seeyouincourt.model;

import androidx.annotation.NonNull;

import com.example.seeyouincourt.model.constants.CaseCategory;
import com.example.seeyouincourt.model.constants.Court;
import com.example.seeyouincourt.model.constants.ProcessStage;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * @author Raul Bob
 */
public class Dossier extends AbstractEntity implements Serializable {


    private List<Side> sides;
    private List<Meeting> meetings;
    private List<WayOfAttack> waysOfAttack;
    private String name;
    private String number;
    private String oldNumber;
    private LocalDate date;
    private Court court;
    private String department;
    private CaseCategory caseCategory;
    private ProcessStage processStage;
    private String object;
    private LocalDate modificationDate;
    private String caseCategoryName;
    private String processStageName;
    private boolean current;

    public List<Side> getSides() {
        return sides;
    }

    public void setSides(List<Side> sides) {
        this.sides = sides;
    }

    public List<Meeting> getMeetings() {
        return meetings;
    }

    public void setMeetings(List<Meeting> meetings) {
        this.meetings = meetings;
    }

    public List<WayOfAttack> getWaysOfAttack() {
        return waysOfAttack;
    }

    public void setWaysOfAttack(List<WayOfAttack> waysOfAttack) {
        this.waysOfAttack = waysOfAttack;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOldNumber() {
        return oldNumber;
    }

    public void setOldNumber(String oldNumber) {
        this.oldNumber = oldNumber;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Court getCourt() {
        return court;
    }

    public void setCourt(Court court) {
        this.court = court;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public CaseCategory getCaseCategory() {
        return caseCategory;
    }

    public void setCaseCategory(CaseCategory caseCategory) {
        this.caseCategory = caseCategory;
    }

    public ProcessStage getProcessStage() {
        return processStage;
    }

    public void setProcessStage(ProcessStage processStage) {
        this.processStage = processStage;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getCaseCategoryName() {
        return caseCategoryName;
    }

    public void setCaseCategoryName(String caseCategoryName) {
        this.caseCategoryName = caseCategoryName;
    }

    public String getProcessStageName() {
        return processStageName;
    }

    public void setProcessStageName(String processStudyName) {
        this.processStageName = processStudyName;
    }

    public boolean isCurrent() {
        return current;
    }

    public void setCurrentAsInt(int primary) {
        this.current = primary != 0;
    }

    public int getCurrentAsInt() {
        return current ? 1 : 0;
    }

    public void setCurrent(boolean current) {
        this.current = current;
    }

    @NonNull
    @Override
    public String toString() {
        return "Dossier{" +
                "name='" + name + '\'' +
                ", sides=" + sides +
                ", meetings=" + meetings +
                ", number='" + number + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Dossier dossier = (Dossier) o;
        return current == dossier.current && Objects.equals(sides, dossier.sides) && Objects.equals(meetings, dossier.meetings) && Objects.equals(waysOfAttack, dossier.waysOfAttack) && Objects.equals(name, dossier.name) && Objects.equals(number, dossier.number) && Objects.equals(oldNumber, dossier.oldNumber) && Objects.equals(date, dossier.date) && court == dossier.court && Objects.equals(department, dossier.department) && caseCategory == dossier.caseCategory && processStage == dossier.processStage && Objects.equals(object, dossier.object) && Objects.equals(modificationDate, dossier.modificationDate) && Objects.equals(caseCategoryName, dossier.caseCategoryName) && Objects.equals(processStageName, dossier.processStageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), sides, meetings, waysOfAttack, name, number, oldNumber, date, court, department, caseCategory, processStage, object, modificationDate, caseCategoryName, processStageName, current);
    }

}
