package com.example.seeyouincourt.service.util;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.seeyouincourt.model.Dossier;

import java.util.List;

public class DossierUtil {

    @RequiresApi(api = Build.VERSION_CODES.O)
    @NonNull
    public static Dossier getLastModificationDateDossier(List<Dossier> dossiers) {
        Dossier maxLastDateDossier = dossiers.get(0);
        for (Dossier dossier : dossiers) {
            if (dossier.getModificationDate().isAfter(maxLastDateDossier.getModificationDate())) {
                maxLastDateDossier = dossier;
            }
        }
        return maxLastDateDossier;
    }
}
