package com.example.seeyouincourt.ui.dossiers;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.service.DossierService;

/**
 * this is a fragment container used by tablets in landscape mode
 */
public class DossierFragmentLandscapeContainer extends Fragment {

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dossiers_fragment_landscape_container, container, false);

        getParentFragmentManager()
                .beginTransaction()
                .replace(R.id.dossiers_list_view_container, new DossierFragment())
                .commit();

        return view;
    }

}
