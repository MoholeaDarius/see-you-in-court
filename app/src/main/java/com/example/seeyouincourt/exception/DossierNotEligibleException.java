package com.example.seeyouincourt.exception;

public class DossierNotEligibleException extends Exception {

    public DossierNotEligibleException() {
        super();
    }

    public DossierNotEligibleException(String message) {
        super(message);
    }

}
