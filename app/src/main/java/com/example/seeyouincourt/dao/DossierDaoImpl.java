package com.example.seeyouincourt.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.seeyouincourt.dao.constants.DossierProperties;
import com.example.seeyouincourt.dao.constants.MeetingProprieties;
import com.example.seeyouincourt.dao.db.MySQLiteOpenHelper;
import com.example.seeyouincourt.model.constants.CaseCategory;
import com.example.seeyouincourt.model.constants.Court;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.model.constants.ProcessStage;

import static com.example.seeyouincourt.dao.constants.DossierProperties.*;
import static com.example.seeyouincourt.util.DateUtil.getDate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class is a simple data base that contains two tables "meetings" and "dossiers"
 */

public class DossierDaoImpl implements DossierDao {

    private static DossierDaoImpl instance;

    private MySQLiteOpenHelper mySQLiteOpenHelper;

    private MeetingDao meetingDao;

    private DossierDaoImpl(Context context) {
        this.mySQLiteOpenHelper = new MySQLiteOpenHelper(context);
        this.meetingDao = MeetingDaoImpl.getInstance(context);
    }

    public static DossierDaoImpl getInstance(Context context) {
        if (instance == null){
            instance = new DossierDaoImpl(context);
        }
        return instance;
    }

    @SuppressLint("NewApi")
    public Optional<List<Dossier>> get() {
        List<Dossier> dossiers = new ArrayList<>();
        SQLiteDatabase db = mySQLiteOpenHelper.getReadableDatabase();
        Cursor cursorCourses = db.rawQuery("SELECT * FROM " + DossierProperties.TABLE_NAME.getValue(), null);
        if (cursorCourses.moveToFirst()) {
            do {
                dossiers.add(buildDossier(cursorCourses));
            } while (cursorCourses.moveToNext());
        }
        cursorCourses.close();
        return Optional.of(dossiers);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void add(Dossier dossier) {
        SQLiteDatabase db = mySQLiteOpenHelper.getWritableDatabase();
        long id = db.insert(DossierProperties.TABLE_NAME.getValue(), null, getContentValues(dossier));
        db.close();
        if (dossier.getMeetings() != null) {
            meetingDao.add(dossier.getMeetings(), id);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void update(Dossier dossier) {
        SQLiteDatabase db = mySQLiteOpenHelper.getWritableDatabase();
        db.update(TABLE_NAME.getValue(), getContentValues(dossier), String.format("%s = ?", "id"), new String[]{dossier.getId() + ""});
        db.close();
    }

    public void deleteAll() {
        SQLiteDatabase db = mySQLiteOpenHelper.getWritableDatabase();
        db.execSQL("DELETE FROM " + DossierProperties.TABLE_NAME.getValue());
        db.close();
    }

    public void delete(Dossier dossier) {
        SQLiteDatabase db = mySQLiteOpenHelper.getWritableDatabase();
        db.delete(MeetingProprieties.TABLE_NAME.getValue(), MeetingProprieties.DOSSIER_ID.getValue() + "= ?", new String[]{dossier.getId() + ""});
        db.delete(DossierProperties.TABLE_NAME.getValue(), DossierProperties.NUMBER.getValue() + "= ?", new String[]{dossier.getNumber() + ""});
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public Optional<List<Dossier>> getLastUpdatedDossiers() {
        List<Dossier> dossiers = new ArrayList<>();
        SQLiteDatabase db = mySQLiteOpenHelper.getReadableDatabase();
        Cursor cursorCourses = db.rawQuery("SELECT * FROM " + DossierProperties.TABLE_NAME.getValue() + " WHERE " + CURRENT_NAME.getValue() + " = " + 1, null);
        if (cursorCourses.moveToFirst()) {
            do {
                Dossier dossier = buildDossier(cursorCourses);
                dossiers.add(dossier);
            } while (cursorCourses.moveToNext());
        }
        cursorCourses.close();
        return Optional.of(dossiers);
    }

    @NonNull
    private ContentValues getContentValues(Dossier dossier) {
        ContentValues values = new ContentValues();
        values.put(NAME.getValue(), dossier.getName());
        values.put(NUMBER.getValue(), dossier.getNumber());
        values.put(OLD_NUMBER.getValue(), dossier.getOldNumber());
        if (dossier.getDate() != null)
            values.put(DossierProperties.DATE.getValue(), dossier.getDate().toString());
        if (dossier.getCourt() != null)
            values.put(COURT.getValue(), dossier.getCourt().getValue());
        values.put(DEPARTMENT.getValue(), dossier.getDepartment());
        if (dossier.getCaseCategory() != null)
            values.put(CASE_CATEGORY.getValue(), dossier.getCaseCategory().getValue());
        if (dossier.getProcessStage() != null)
            values.put(PROCESS_STAGE.getValue(), dossier.getProcessStage().getValue());
        values.put(OBJECT.getValue(), dossier.getObject());
        if (dossier.getModificationDate() != null)
            values.put(MODIFICATION_DATE.getValue(), dossier.getModificationDate().toString());
        values.put(CASE_CATEGORY_NAME.getValue(), dossier.getCaseCategoryName());
        values.put(PROCESS_STAGE_NAME.getValue(), dossier.getProcessStageName());
        values.put(CURRENT_NAME.getValue(), dossier.getCurrentAsInt());
        return values;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @NonNull
    private Dossier buildDossier(Cursor cursorCourses) {
        Dossier dossier = new Dossier();
        dossier.setId(cursorCourses.getInt(0));
        dossier.setName(cursorCourses.getString(1));
        dossier.setNumber(cursorCourses.getString(2));
        dossier.setOldNumber(cursorCourses.getString(3));
        if (cursorCourses.getString(4) != null)
            dossier.setDate(getDate(cursorCourses.getString(4)).toLocalDate());
        if (cursorCourses.getString(5) != null)
            dossier.setCourt(Court.fromValue(cursorCourses.getString(5)));
        dossier.setDepartment(cursorCourses.getString(6));
        if (cursorCourses.getString(7) != null)
            dossier.setCaseCategory(CaseCategory.fromValue(cursorCourses.getString(7)));
        if (cursorCourses.getString(8) != null)
            dossier.setProcessStage(ProcessStage.fromValue(cursorCourses.getString(8)));
        dossier.setObject(cursorCourses.getString(9));
        if (cursorCourses.getString(10) != null)
            dossier.setModificationDate(getDate(cursorCourses.getString(10)).toLocalDate());
        dossier.setCaseCategoryName(cursorCourses.getString(11));
        dossier.setProcessStageName(cursorCourses.getString(12));
        if (meetingDao.getByDossierId(cursorCourses.getInt(0)).isPresent())
            dossier.setMeetings(meetingDao.getByDossierId(cursorCourses.getInt(0)).get());
        dossier.setCurrentAsInt(cursorCourses.getInt(13));
        return dossier;
    }

}
