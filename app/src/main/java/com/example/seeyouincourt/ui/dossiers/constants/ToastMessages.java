package com.example.seeyouincourt.ui.dossiers.constants;

public enum ToastMessages {

    DOSSIER_FIELD_IS_EMPTY("Dossier no field can't be empty!"),
    INVALID_DOSSIER_NO("Invalid dossier number!"),
    DOSSIER_ALREADY_EXISTS("You already added this dossier!"),
    DOSSIER_ADDED_SUCCESSFULLY("Dossier added successfully!"),
    NO_INTERNET_CONNECTION("No internet connection!"),
    ALIAS_CHANGED_SUCCESSFULLY("Alias changed successfully!"),
    ;

    private final String value;

    ToastMessages(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
