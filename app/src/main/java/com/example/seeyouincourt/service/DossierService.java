package com.example.seeyouincourt.service;

import static com.example.seeyouincourt.service.util.DossierUtil.getLastModificationDateDossier;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.seeyouincourt.dao.DossierDao;
import com.example.seeyouincourt.dao.DossierDaoImpl;
import com.example.seeyouincourt.dao.MeetingDao;
import com.example.seeyouincourt.dao.MeetingDaoImpl;
import com.example.seeyouincourt.model.CalendarMeeting;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.model.Meeting;
import com.example.seeyouincourt.util.DateUtil;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * This class make the connection between DAO and UI.
 */
public class DossierService implements Serializable {

    private static DossierService instance;

    private final DossierDao dossierDao;

    private final MeetingDao meetingDao;

    private DossierService(Context context) {
        this.dossierDao = DossierDaoImpl.getInstance(context);
        this.meetingDao = MeetingDaoImpl.getInstance(context);
    }

    public static DossierService getInstance(Context context) {
        if(instance == null){
            instance = new DossierService(context);
        }
        return instance;
    }

    public Optional<List<Dossier>> getDossiers() {
        return dossierDao.get();
    }

    public void updateDossier(Dossier dossier) {
        dossierDao.update(dossier);
    }

    public void addNewDossier(Dossier dossier) {
        dossierDao.add(dossier);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void addNewDossiers(List<Dossier> dossiers) {
        if (dossiers == null || dossiers.isEmpty()) {
            return;
        }
        Dossier maxLastDateDossier = getLastModificationDateDossier(dossiers);
        maxLastDateDossier.setCurrent(true);
        for (Dossier dossier : dossiers) {
            dossierDao.add(dossier);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public List<Dossier> getCurrentDossiers() {
        return dossierDao.getLastUpdatedDossiers().orElse(new ArrayList<>());
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<Dossier> getDossiersByNo(String dossierNo) {
        List<Dossier> dossiers = new ArrayList<>();
        if (getDossiers().isPresent())
            for (Dossier dossier : getDossiers().get()) {
                if (dossier.getNumber().equals(dossierNo))
                    dossiers.add(dossier);
            }
        return dossiers;
    }

    /**
     * create a pojo from dossiers and meetings
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<CalendarMeeting> getCalendarMeetings() {
        List<CalendarMeeting> calendarMeetings = new ArrayList<>();
        Optional<List<Dossier>> dossiersOptional = getDossiers();

        if (dossiersOptional.isPresent()) {
            for (Dossier dossier : dossiersOptional.get()) {
                if (dossier.getMeetings() != null)
                    for (Meeting meeting : dossier.getMeetings()) {
                        calendarMeetings.add(new CalendarMeeting(meeting.getDate(), dossier.getCourt(), dossier.getNumber(), dossier.getName(), meeting.getComplete(), meeting.getSolution(), meeting.getSolutionSummary(), dossier.getId(), meeting.isExpended()));
                    }
            }
        }
        return calendarMeetings;
    }

    /**
     * @return - sorted meetings of today + all future ones
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public List<CalendarMeeting> getFutureSortedCalendarMeetings() {
        List<CalendarMeeting> futureMeetings = new ArrayList<>();
        getCalendarMeetings().forEach(meeting -> {
            LocalDate yesterday = LocalDate.now().minusDays(1);
            if (meeting.getDate().toLocalDate().isAfter(yesterday)) {
                futureMeetings.add(meeting);
            }
        });

        futureMeetings.sort(Comparator.comparing(CalendarMeeting::getDate));
        return futureMeetings;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public List<CalendarMeeting> getFutureCalendarMeetings() {
        List<CalendarMeeting> futureCalendarMeetings = new ArrayList<>();
        LocalDate localDate = DateUtil.getNextWorkingDates(5).get(4);
        getFutureSortedCalendarMeetings().forEach(calendarMeeting -> {
            if (calendarMeeting.getDate().toLocalDate().isAfter(localDate))
                futureCalendarMeetings.add(calendarMeeting);
        });
        return futureCalendarMeetings;
    }


    public void deleteAllMeetings() {
        meetingDao.deleteAll();
    }

    public void deleteAllDossiers() {
        dossierDao.deleteAll();
    }

    public Optional<List<Meeting>> getMeetings() {
        return meetingDao.get();
    }

    public void deleteDossiers(List<Dossier> dossiers) {
        for (Dossier dossier : dossiers) {
            dossierDao.delete(dossier);
        }
    }

}
