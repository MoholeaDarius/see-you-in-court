package com.example.seeyouincourt.ui.dossiers.adapters;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.model.Meeting;

import java.util.List;

/**
 * This class is a adapter for Meetings.
 */
public class MeetingAdapter extends RecyclerView.Adapter<MeetingAdapter.MeetingVH> {

    private List<Meeting> meetings;

    public MeetingAdapter(List<Meeting> meetings) {
        this.meetings = meetings;
    }

    @NonNull
    @Override
    public MeetingVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.meeting_item, parent, false);
        return new MeetingVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MeetingVH holder, int position) {
        Meeting meeting = meetings.get(position);
        holder.dateTextView.setText(setDate(position));
        if (meeting.getComplete().equals("anyType{}")) {
            holder.completeNameTextView.setVisibility(View.GONE);
            holder.completeTextView.setVisibility(View.GONE);
        } else {
            holder.completeNameTextView.setVisibility(View.VISIBLE);
            holder.completeTextView.setVisibility(View.VISIBLE);
            holder.completeTextView.setText(meeting.getComplete());
        }
        if (meeting.getSolution().equals("anyType{}")) {
            holder.solutionNameTextView.setVisibility(View.GONE);
            holder.solutionTextView.setVisibility(View.GONE);
        } else {
            holder.solutionNameTextView.setVisibility(View.VISIBLE);
            holder.solutionTextView.setVisibility(View.VISIBLE);
            holder.solutionTextView.setText(meeting.getSolution());
        }
        if (meeting.getSolutionSummary().equals("anyType{}")) {
            holder.imageView.setVisibility(View.GONE);
        } else {
            holder.imageView.setVisibility(View.VISIBLE);
        }
        holder.solutionSummaryTextView.setText(meeting.getSolutionSummary());
        boolean isExpanded = meetings.get(position).isExpended();
        holder.expandableLayout.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
    }

    private String setDate(int position) {
        String[] fullDate = meetings.get(position).getDate().toString().split("T");
        String[] date = fullDate[0].split("-");
        String date1 = date[2] + "." + date[1] + "." + date[0];
        return date1 + " - " + fullDate[1];
    }


    @Override
    public int getItemCount() {
        return meetings.size();
    }

    class MeetingVH extends RecyclerView.ViewHolder {

        private final LinearLayout expandableLayout;
        private final TextView dateTextView;
        private final TextView completeTextView;
        private final TextView completeNameTextView;
        private final TextView solutionTextView;
        private final TextView solutionNameTextView;
        private final TextView solutionSummaryTextView;
        private final ImageView imageView;

        public MeetingVH(@NonNull final View itemView) {
            super(itemView);
            dateTextView = itemView.findViewById(R.id.time_text_view);
            completeTextView = itemView.findViewById(R.id.complete_text_view);
            completeNameTextView = itemView.findViewById(R.id.complete_name_text_view);
            solutionTextView = itemView.findViewById(R.id.solution_text_view);
            solutionNameTextView = itemView.findViewById(R.id.solution_name_text_view);
            solutionSummaryTextView = itemView.findViewById(R.id.solution_summary_text_view);
            expandableLayout = itemView.findViewById(R.id.expendable_layout);
            LinearLayout detailsLayout = itemView.findViewById(R.id.details_layout);
            imageView = itemView.findViewById(R.id.image_meeting_item);

            solutionSummaryTextView.setOnClickListener(view -> {
                Meeting meeting = meetings.get(getAdapterPosition());
                meeting.setExpended(!meeting.isExpended());
                notifyItemChanged(getAdapterPosition());
            });

            detailsLayout.setOnClickListener(view -> {
                Meeting meeting = meetings.get(getAdapterPosition());
                if (!meeting.getSolutionSummary().equals("anyType{}")) {
                    meeting.setExpended(!meeting.isExpended());
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }

}
