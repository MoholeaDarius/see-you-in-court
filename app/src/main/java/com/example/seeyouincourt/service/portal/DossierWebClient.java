package com.example.seeyouincourt.service.portal;

import com.example.seeyouincourt.exception.*;
import com.example.seeyouincourt.model.Dossier;

import java.io.IOException;
import java.util.List;

/**
 * Client that calls Court Cases Web Service Portal: http://portal.just.ro.
 *
 * @author Raul Bob
 */
public interface DossierWebClient {

    /**
     * This method calls #portalquery.just.ro/CautareDosare web service method.
     */

    List<Dossier> getDossiers(String dossierNo) throws JustPortalException;

}
