package com.example.seeyouincourt.ui.meetings.constants;

public enum DayOfWeek {
    Luni( "Monday"),
    Marti( "Tuesday"),
    Miercuri( "Wednesday"),
    Joi( "Thursday"),
    Vineri( "Friday"),
    Sambata( "Saturday"),
    Duminica( "Sunday");

    private final String value;

    DayOfWeek(String value) {

        this.value = value;
    }

    public String getValue() {
        return value;
    }


    public static String findValue(String dayOfWeekENG) {
        for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
            if (dayOfWeek.getValue().equalsIgnoreCase(dayOfWeekENG)) {
                return dayOfWeek.getValue();
            }
        }
        throw new IllegalArgumentException(dayOfWeekENG);
    }

}
