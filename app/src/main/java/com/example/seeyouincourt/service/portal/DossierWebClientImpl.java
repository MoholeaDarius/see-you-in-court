package com.example.seeyouincourt.service.portal;

import android.annotation.SuppressLint;

import com.example.seeyouincourt.exception.*;
import com.example.seeyouincourt.model.constants.CaseCategory;
import com.example.seeyouincourt.model.constants.Court;
import com.example.seeyouincourt.model.Dossier;
import com.example.seeyouincourt.model.Meeting;
import com.example.seeyouincourt.model.constants.MeetingDocument;
import com.example.seeyouincourt.model.constants.ProcessStage;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class DossierWebClientImpl implements DossierWebClient {

    public static final String URL = "http://portalquery.just.ro/query.asmx?WSDL";
    public static final String NAMESPACE = "portalquery.just.ro";
    public static final String METHOD = "CautareDosare";
    public static final String SOAP_ACTION = "portalquery.just.ro/CautareDosare";

    @Override
    public List<Dossier> getDossiers(String dossierNo) throws JustPortalException {
        return getDossiers(dossierNo, null, null, null, null, null);
    }

    private List<Dossier> getDossiers(String dossierNo, String dossierObject, String sideName, Court court,
                                      LocalDate startDate, LocalDate stopDate) throws JustPortalException {
        SoapObject request = new SoapObject(NAMESPACE, METHOD);
        request.addProperty("numarDosar", dossierNo);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        envelope.dotNet = true;

        try {
            HttpTransportSE transport = new HttpTransportSE(URL);
            transport.call(SOAP_ACTION, envelope);
            return soapResult((SoapObject) envelope.bodyIn);
        } catch (IOException | XmlPullParserException | JustPortalException e) {
            throw new JustPortalException(e.getMessage());
        }
    }

    @SuppressLint("NewApi")
    public List<Dossier> soapResult(SoapObject response) throws JustPortalException {
        if (response.getPropertyCount() == 0) throw new JustPortalException();
        SoapObject dossierListObject = (SoapObject) response.getProperty(0);
        List<Dossier> list = new ArrayList<>();
        for (int i = 0; i < dossierListObject.getPropertyCount(); i++) {
            SoapObject dossierObject = (SoapObject) dossierListObject.getProperty(i);
            Dossier portalDossier = new Dossier();
            try {
                portalDossier.setMeetings(getMeetings((SoapObject) dossierObject.getProperty("sedinte")));
            } catch (Exception e) {
                System.err.println("Nu exista proprietatea sedinte in acest dosar!");
            }
            portalDossier.setNumber(dossierObject.getProperty("numar").toString());
            portalDossier.setOldNumber(dossierObject.getProperty("numarVechi").toString());
            String[] fullDate = dossierObject.getProperty("data").toString().split("T");
            portalDossier.setDate(getDate(dossierObject.getProperty("data").toString(), fullDate[1]).toLocalDate());
            portalDossier.setCourt(Court.fromValue(dossierObject.getProperty("institutie").toString()));
            portalDossier.setDepartment(dossierObject.getProperty("departament").toString());
            portalDossier.setCaseCategory(CaseCategory.fromValue(dossierObject.getProperty("categorieCaz").toString()));
            portalDossier.setProcessStage(ProcessStage.fromValue(dossierObject.getProperty("stadiuProcesual").toString()));
            portalDossier.setObject(dossierObject.getProperty("obiect").toString());
            String[] modifiedDate = dossierObject.getProperty("dataModificare").toString().split("T");
            portalDossier.setModificationDate(getDate(dossierObject.getProperty("dataModificare").toString(), modifiedDate[1]).toLocalDate());
            portalDossier.setCaseCategoryName(dossierObject.getProperty("categorieCazNume").toString());
            portalDossier.setProcessStageName(dossierObject.getProperty("stadiuProcesualNume").toString());
            list.add(portalDossier);
        }

        return list;
    }

    @SuppressLint("NewApi")
    private List<Meeting> getMeetings(SoapObject meetingsObject) {
        List<Meeting> meetings = new ArrayList<>();
        for (int i = 0; i < meetingsObject.getPropertyCount(); i++) {
            SoapObject meetingObject = (SoapObject) meetingsObject.getProperty(i);
            Meeting meeting = new Meeting();
            meeting.setComplete(meetingObject.getProperty("complet").toString());
            meeting.setDate(getDate(meetingObject.getProperty("data").toString(), meetingObject.getProperty("ora").toString()));
            meeting.setSolution(meetingObject.getProperty("solutie").toString());
            meeting.setSolutionSummary(meetingObject.getProperty("solutieSumar").toString());
            if (meetingObject.getProperty("dataPronuntare") != null) {
                String[] pronouncementDate = meetingObject.getProperty("dataPronuntare").toString().split("T");
                meeting.setPronouncementDate(getDate(meetingObject.getProperty("dataPronuntare").toString(), pronouncementDate[1]).toLocalDate());
            }
            if (meetingObject.getProperty("documentSedinta") != null) {
                meeting.setMeetingDocument(MeetingDocument.fromValue(meetingObject.getProperty("documentSedinta").toString()));
            }
            if (meetingObject.getProperty("numarDocument") != null) {
                meeting.setDocumentNo(meetingObject.getProperty("numarDocument").toString());
            }
            if (meetingObject.getProperty("dataDocument") != null) {
                String[] documentDate = meetingObject.getProperty("dataDocument").toString().split("T");
                meeting.setDocumentDate(getDate(meetingObject.getProperty("dataDocument").toString(), documentDate[1]).toLocalDate());
            }
            meetings.add(meeting);
        }
        return meetings;
    }

    @SuppressLint("NewApi")
    private LocalDateTime getDate(String date, String hour) {
        String[] hours = hour.split(":");
        if (hours[0].length() == 1) hours[0] = "0" + hours[0];
        String realHour = "";
        for (int i = 0; i < hours.length; i++) {
            if (i == hours.length - 1) {
                realHour += hours[i];
            } else realHour += hours[i] + ":";
        }
        return LocalDateTime.parse(date).toLocalDate().atTime(LocalTime.parse(realHour));
    }

}
