package com.example.seeyouincourt.ui.meetings;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.example.seeyouincourt.R;
import com.example.seeyouincourt.service.DossierService;
import com.example.seeyouincourt.ui.meetings.adapter.*;
import com.google.android.material.tabs.TabLayout;

/**
 * Meetings fragment contains a view pager.
 */

public class MeetingsFragment extends Fragment {

    private TabLayout tabLayout;
    private ViewPager2 viewPager;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dossier_meetings_fragment, container, false);

        initViews(view);

        MeetingFragmentAdapter fragmentAdapter = new MeetingFragmentAdapter(getChildFragmentManager(), getLifecycle());
        viewPager.setAdapter(fragmentAdapter);

        addTabsToTabLayout();

        selectedTabListener();

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });
        viewPager.setSaveEnabled(false);

        return view;
    }

    private void initViews(View view) {
        tabLayout = view.findViewById(R.id.meetings_tab_layout);
        viewPager = view.findViewById(R.id.meetings_view_pager);
    }

    private void addTabsToTabLayout() {
        tabLayout.addTab(tabLayout.newTab().setText(R.string.calendar));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.list));
    }

    private void selectedTabListener() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}
