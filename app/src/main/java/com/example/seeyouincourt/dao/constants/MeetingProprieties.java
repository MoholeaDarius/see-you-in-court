package com.example.seeyouincourt.dao.constants;

import com.example.seeyouincourt.dao.DossierDaoImpl;

/**
 * Local enum for {@link DossierDaoImpl}.
 * Contains column names and a quarry (create table)
 */

public enum MeetingProprieties {

    TABLE_NAME("Meetings"),
    ID("id"),
    COMPETE("complete"),
    DATE("date"),
    SOLUTION("solution"),
    SOLUTION_SUMMARY("solutionSummary"),
    PRONOUNCEMENT_DATE("pronouncementDate"),
    DOCUMENT("document"),
    DOCUMENT_NO("documentNO"),
    DOCUMENT_DATE("documentDate"),
    WARNINGS("warnings"),
    OBSERVATIONS("observations"),
    DOSSIER_ID("dossier_id"),

    CREATE_TABLE_SQL("CREATE TABLE " + TABLE_NAME.getValue() + " ("
            + ID.getValue() + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COMPETE.getValue() + " TEXT,"
            + DATE.getValue() + " TEXT,"
            + SOLUTION.getValue() + " TEXT,"
            + SOLUTION_SUMMARY.getValue() + " TEXT,"
            + PRONOUNCEMENT_DATE.getValue() + " TEXT,"
            + DOCUMENT.getValue() + " TEXT,"
            + DOCUMENT_NO.getValue() + " TEXT,"
            + DOCUMENT_DATE.getValue() + " TEXT,"
            + WARNINGS.getValue() + " TEXT,"
            + OBSERVATIONS.getValue() + " TEXT,"
            + DOSSIER_ID.getValue() + " INTEGER,"
            + "FOREIGN KEY (dossier_id) REFERENCES dossiers(id))");


    private final String value;

    MeetingProprieties(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static MeetingProprieties fromValue(String v) {
        for (MeetingProprieties mP : MeetingProprieties.values()) {
            if (mP.getValue().equalsIgnoreCase(v)) {
                return mP;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
