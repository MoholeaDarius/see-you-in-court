package com.example.seeyouincourt.dao;

import static com.example.seeyouincourt.dao.constants.MeetingProprieties.COMPETE;
import static com.example.seeyouincourt.dao.constants.MeetingProprieties.DOCUMENT;
import static com.example.seeyouincourt.dao.constants.MeetingProprieties.DOCUMENT_DATE;
import static com.example.seeyouincourt.dao.constants.MeetingProprieties.DOCUMENT_NO;
import static com.example.seeyouincourt.dao.constants.MeetingProprieties.DOSSIER_ID;
import static com.example.seeyouincourt.dao.constants.MeetingProprieties.OBSERVATIONS;
import static com.example.seeyouincourt.dao.constants.MeetingProprieties.PRONOUNCEMENT_DATE;
import static com.example.seeyouincourt.dao.constants.MeetingProprieties.SOLUTION;
import static com.example.seeyouincourt.dao.constants.MeetingProprieties.SOLUTION_SUMMARY;
import static com.example.seeyouincourt.dao.constants.MeetingProprieties.WARNINGS;
import static com.example.seeyouincourt.util.DateUtil.getDate;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.seeyouincourt.dao.constants.MeetingProprieties;
import com.example.seeyouincourt.dao.db.MySQLiteOpenHelper;
import com.example.seeyouincourt.model.Meeting;
import com.example.seeyouincourt.model.constants.MeetingDocument;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MeetingDaoImpl implements MeetingDao {

    private static MeetingDaoImpl instance;

    private MySQLiteOpenHelper mySQLiteOpenHelper;

    private MeetingDaoImpl(Context context) {
        this.mySQLiteOpenHelper = new MySQLiteOpenHelper(context);
    }

    public static MeetingDaoImpl getInstance(Context context) {
        if(instance == null){
            instance = new MeetingDaoImpl(context);
        }
        return instance;
    }

    @SuppressLint("NewApi")
    public Optional<List<Meeting>> get() {
        List<Meeting> meetings = new ArrayList<>();
        SQLiteDatabase db = mySQLiteOpenHelper.getReadableDatabase();
        Cursor cursorCourses = db.rawQuery("SELECT * FROM " + MeetingProprieties.TABLE_NAME.getValue(), null);
        if (cursorCourses.moveToFirst()) {
            do {
                meetings.add(buildMeeting(cursorCourses));
            } while (cursorCourses.moveToNext());
        }
        cursorCourses.close();
        return Optional.of(meetings);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void add(List<Meeting> meetings, long dossierId) {
        if (meetings == null || meetings.isEmpty()) {
            return;
        }
        SQLiteDatabase db = mySQLiteOpenHelper.getWritableDatabase();
        for (Meeting meeting : meetings) {
            db.insert(MeetingProprieties.TABLE_NAME.getValue(), null, getContentValues(meeting, dossierId));
        }
        db.close();
    }

    @SuppressLint("NewApi")
    public Optional<List<Meeting>> getByDossierId(int dossierId) {
        List<Meeting> meetings = new ArrayList<>();
        SQLiteDatabase db = mySQLiteOpenHelper.getReadableDatabase();
        Cursor cursorCourses = db.rawQuery("SELECT * FROM " + MeetingProprieties.TABLE_NAME.getValue() + " WHERE " + MeetingProprieties.DOSSIER_ID.getValue() + " = " + dossierId, null);
        if (cursorCourses.moveToFirst()) {
            do {
                Meeting meeting = buildMeeting(cursorCourses);
                meetings.add(meeting);
            } while (cursorCourses.moveToNext());
        }
        cursorCourses.close();
        return Optional.of(meetings);
    }

    public void deleteAll() {
        SQLiteDatabase db = mySQLiteOpenHelper.getWritableDatabase();
        db.execSQL("DELETE FROM " + MeetingProprieties.TABLE_NAME.getValue());
        db.close();
    }

    private ContentValues getContentValues(Meeting meeting, long dossierId) {
        ContentValues values = new ContentValues();
        values.put(COMPETE.getValue(), meeting.getComplete());
        if (meeting.getDate() != null)
            values.put(MeetingProprieties.DATE.getValue(), meeting.getDate().toString());
        values.put(SOLUTION.getValue(), meeting.getSolution());
        values.put(SOLUTION_SUMMARY.getValue(), meeting.getSolutionSummary());
        if (meeting.getPronouncementDate() != null)
            values.put(PRONOUNCEMENT_DATE.getValue(), meeting.getPronouncementDate().toString());
        if (meeting.getDocument() != null)
            values.put(DOCUMENT.getValue(), meeting.getDocument().getValue());
        values.put(DOCUMENT_NO.getValue(), meeting.getDocumentNo());
        if (meeting.getDocumentDate() != null)
            values.put(DOCUMENT_DATE.getValue(), meeting.getDocumentDate().toString());
        values.put(WARNINGS.getValue(), meeting.getWarnings());
        values.put(OBSERVATIONS.getValue(), meeting.getObservations());
        values.put(DOSSIER_ID.getValue(), dossierId);
        return values;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private Meeting buildMeeting(Cursor cursorCourses) {
        Meeting meeting = new Meeting();
        meeting.setId(cursorCourses.getInt(0));
        meeting.setComplete(cursorCourses.getString(1));
        if (cursorCourses.getString(2) != null)
            meeting.setDate(getDate(cursorCourses.getString(2)));
        meeting.setSolution(cursorCourses.getString(3));
        meeting.setSolutionSummary(cursorCourses.getString(4));
        if (cursorCourses.getString(5) != null)
            meeting.setPronouncementDate(getDate(cursorCourses.getString(5)).toLocalDate());
        if (cursorCourses.getString(6) != null)
            meeting.setMeetingDocument(MeetingDocument.fromValue(cursorCourses.getString(6)));
        meeting.setDocumentNo(cursorCourses.getString(7));
        if (cursorCourses.getString(8) != null)
            meeting.setDocumentDate(getDate(cursorCourses.getString(8)).toLocalDate());
        meeting.setWarnings(cursorCourses.getString(9));
        meeting.setObservations(cursorCourses.getString(10));
        return meeting;
    }

}
