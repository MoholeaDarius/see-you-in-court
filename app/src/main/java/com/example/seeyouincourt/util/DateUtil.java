package com.example.seeyouincourt.util;

import static java.time.DayOfWeek.FRIDAY;
import static java.time.DayOfWeek.SATURDAY;

import android.annotation.SuppressLint;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.seeyouincourt.model.CalendarMeeting;
import com.example.seeyouincourt.ui.meetings.constants.DayOfWeek;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RequiresApi(api = Build.VERSION_CODES.O)
public class DateUtil {

    public static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    public static final DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH:mm");
    public static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyy - HH:mm:ss");
    public static final String TODAY = "Today";
    public static final String TOMORROW = "Tomorrow";

    @SuppressLint("NewApi")
    public static LocalDateTime getDate(String date) {
        if (date.length() < 11) {
            String fullDate = date + "T00:00:00";
            return LocalDateTime.parse(fullDate).toLocalDate().atTime(LocalTime.parse("00:00:00"));
        }
        String[] hours = date.split("T");
        return LocalDateTime.parse(date).toLocalDate().atTime(LocalTime.parse(hours[1]));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static List<LocalDate> getNextWorkingDates(int noOfDays) {
        List<LocalDate> next5Days = new ArrayList<>();
        LocalDate workingDate = nextWorkingDate(LocalDate.now().minusDays(1));
        for (int i = 0; i < noOfDays; i++) {
            next5Days.add(workingDate);
            workingDate = nextWorkingDate(workingDate);
        }
        return next5Days;
    }

    /**
     * @param localDate
     * @return - next working date related to localDate
     */
    private static LocalDate nextWorkingDate(LocalDate localDate) {
        if (localDate.getDayOfWeek().equals(FRIDAY)) {
            return localDate.plusDays(3);
        } else if (localDate.getDayOfWeek().equals(SATURDAY)) {
            return localDate.plusDays(2);
        }
        return localDate.plusDays(1);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static List<CalendarMeeting> getCalendarMeetingsByMonth(List<CalendarMeeting> calendarMeetings, int monthValue) {
        List<CalendarMeeting> calendarMeetingsSorted = new ArrayList<>();
        for (CalendarMeeting calendarMeeting : calendarMeetings) {
            if (calendarMeeting.getDate().getMonthValue() == monthValue) {
                calendarMeetingsSorted.add(calendarMeeting);
            }
        }
        return calendarMeetingsSorted;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static List<CalendarMeeting> getCalendarMeetingsByDate(List<CalendarMeeting> calendarMeetings, LocalDate localDate) {
        List<CalendarMeeting> calendarMeetingsSorted = new ArrayList<>();
        for (CalendarMeeting calendarMeeting : calendarMeetings) {
            if (calendarMeeting.getDate().toLocalDate().equals(localDate)) {
                calendarMeetingsSorted.add(calendarMeeting);
            }
        }
        return calendarMeetingsSorted;
    }

}
